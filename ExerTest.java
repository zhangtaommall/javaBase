package com.candy.IO流.exer;

import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author Candy
 * @create 2020-09-02 15:00
 * 图片加密操作
 */
public class ExerTest {

    /**
     * bytes[i] = (byte)(bytes[i] ^ 5);可以改变字节的数值达到加密效果
     * 如果再次调用，则可以恢复原来的数值(m ^ n ^ n)异或两次可以恢复原来的数值
     */
    @Test
    public void test(){
        File file1 = new File("吃烧烤的小姐姐2.jpg");
        File file2 = new File("吃烧烤的小姐姐3.jpg");

        FileInputStream fileInputStream = null;
        FileOutputStream fileOutputStream = null;
        try {
            fileInputStream = new FileInputStream(file1);
            fileOutputStream = new FileOutputStream(file2);
            byte[] bytes = new byte[1024];
            int len;
            while ((len = fileInputStream.read(bytes)) != -1){
                for (int i = 0; i < bytes.length; i++) {
                    bytes[i] = (byte)(bytes[i] ^ 5);
                }
                fileOutputStream.write(bytes,0,len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(fileOutputStream != null){
                    fileOutputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if(fileInputStream != null){
                    fileInputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
