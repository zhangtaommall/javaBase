package com.candy.IO流;

import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * File的常用方法
 * @author Candy
 * @create 2020-08-26 15:31
 */
public class FileTest {

    /**
     * 1.public File(String pathname) 以pathname为路径创建File对象,可以是绝对路径或者相对路径
     * 如果pathname是相对路径,则默认的当前路径在系统属性user.dir中存储。
     *       绝对路径：是一个固定的路径,从盘符开始
     *       相对路径：是相对于某个位置开始
     * 2.public File(String parent,String child) 以parent为父路径，child为子路径创建File对象。
     * 3.public File(File parent,String child) 根据一个父File对象和子文件路径创建File对象
     */
    @Test
    public void test(){
        /**
         * 因为每个系统直接的分隔符不一样，例如："\"、"\\"、"/"等，使用JAVA提供的统一分隔符
         * File.separator
         */
        //构造器一
        File file = new File("hello.txt");
        File file2 = new File("E:"+File.separator+"javaBase");
        System.out.println(file);

        //构造器二
        File filetow = new File(file2,"hello.txt");
    }

    /**
     *  File类的获取功能
     *  public String getAbsolutePath()：获取绝对路径
     *  public String getPath() ：获取路径
     *  public String getName() ：获取名称
     *  public String getParent()：获取上层文件目录路径。若无，返回null
     *  public long length() ：获取文件长度（即：字节数）。不能获取目录的长度。
     *  public long lastModified() ：获取最后一次的修改时间，毫秒值
     *
     *  public String[] list() ：获取指定目录下的所有文件或者文件目录的名称数组(不包含子目录)
     *  public File[] listFiles() ：获取指定目录下的所有文件或者文件目录的File数组
     */
    @Test
    public void test2(){
        File file = new File("hello.txt");
        File file2 = new File("E:"+File.separator+"filetest"+File.separator+"hi.txt");

        System.out.println(file.getAbsolutePath());
        System.out.println(file.getPath());
        System.out.println(file.getName());
        System.out.println(file.getParent());
        System.out.println(file.length());
        System.out.println(file.lastModified());
        System.out.println();
        File file3 = new File("D:\\BaiduNetdiskDownload");
        String[] list = file3.list();
        for (String s : list) {
            System.out.println(s);
        }
    }

    /**
     *  public boolean isDirectory()：判断是否是文件目录
     *  public boolean isFile() ：判断是否是文件
     *  public boolean exists() ：判断文件是否存在
     *  public boolean canRead() ：判断是否可读
     *  public boolean canWrite() ：判断是否可写
     *  public boolean isHidden() ：判断是否隐藏
     *  Flie类的创建方法
     *   public boolean createNewFile() ：创建文件。若文件存在，则不创建，返回false
     *   public boolean mkdir() ：创建文件目录。如果此文件目录存在，就不创建了。
     *      如果此文件目录的上层目录不存在，也不创建。
     *   public boolean mkdirs() ：创建文件目录。如果上层文件目录不存在，一并创建
     * 注意事项：如果你创建文件或者文件目录没有写盘符路径，那么，默认在项目 路径下。
     *   public boolean delete()：删除文件或者文件夹
     */
    @Test
    public void test3() throws Exception{
        File file3 = new File("D:\\BaiduNetdiskDownload");
        System.out.println(file3.isDirectory());
        System.out.println(file3.isFile());
        System.out.println(file3.exists());

        //文件创建相关
        File file = new File("hello1.txt");
        if(!file.exists()){
            file.createNewFile();
            System.out.println("创建成功");
        }else{
            file.delete();
            System.out.println("删除成功");
        }
    }

    /**
     * 使用递归遍历某个文件目录里面所有的文件(包括子目录)
     */
    @Test
    public void testDir(){
        List<File> fileList = new ArrayList<>();
        File file2 = new File("D:\\BaiduNetdiskDownload");
        ForFile(fileList, file2);
        long length = 0L;
        for (File file : fileList) {
            length += file.length();
        }
        System.out.println(length);


    }

    /**
     * 递归算法
     * @param fileList
     * @param dir
     */
    private void ForFile(List<File> fileList,File dir){
        File[] files = dir.listFiles();
        for (File file : files) {
            if(file.isDirectory()){
                ForFile(fileList,file);
            }else{
                fileList.add(file);
            }
        }
    }

}
