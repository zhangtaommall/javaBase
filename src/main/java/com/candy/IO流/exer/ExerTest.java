package com.candy.IO流.exer;

import org.junit.Test;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author Candy
 * @create 2020-09-02 15:00
 * 图片加密操作
 */
public class ExerTest {

    /**
     * bytes[i] = (byte)(bytes[i] ^ 5);可以改变字节的数值达到加密效果
     * 如果再次调用，则可以恢复原来的数值(m ^ n ^ n)异或两次可以恢复原来的数值
     */
    @Test
    public void test(){
        File file1 = new File("吃烧烤的小姐姐2.jpg");
        File file2 = new File("吃烧烤的小姐姐3.jpg");

        FileInputStream fileInputStream = null;
        FileOutputStream fileOutputStream = null;
        try {
            fileInputStream = new FileInputStream(file1);
            fileOutputStream = new FileOutputStream(file2);
            byte[] bytes = new byte[1024];
            int len;
            while ((len = fileInputStream.read(bytes)) != -1){
                for (int i = 0; i < bytes.length; i++) {
                    bytes[i] = (byte)(bytes[i] ^ 5);
                }
                fileOutputStream.write(bytes,0,len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(fileOutputStream != null){
                    fileOutputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if(fileInputStream != null){
                    fileInputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 获取文本中每个字符出现的次数
     */
    @Test
    public void test2(){
        Map<Character,Integer> map = new HashMap<>();
        File file = new File("ExerTest.java");
        FileReader fileReader = null;
        try {
            fileReader = new FileReader(file);
            int len;
            while((len = fileReader.read()) != -1){
                char c = (char) len;
                //空格(' ')，tab键(\t)，回车键(\r)，换行键(\n),空白(Character.MIN_VALUE)
                if(c == '\n' || c == '\r' || c == '\t'
                        || c == ' ' || c == Character.MIN_VALUE || c == '-'){
                    continue;
                }
                if(!map.containsKey(c)){
                    map.put(c,1);
                }else{
                    Integer integer = map.get(c);
                    map.put(c,++integer);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(fileReader != null){
                    fileReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Set<Map.Entry<Character, Integer>> entries = map.entrySet();
        for (Map.Entry<Character, Integer> entry : entries) {
            Character key = entry.getKey();
            Integer value = entry.getValue();
            System.out.println("字符："+key+"      次数："+value);
        }
    }

}
