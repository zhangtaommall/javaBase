package com.candy.IO流.其他流;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author Candy
 * @create 2020-09-02 17:33
 * 其他的处理流
 * 1.标准的输入、输出流
 * 2.打印流
 * 3.数据流
 */
public class OtherStreamTest {


    /**
     * 标准的输入、输出流
     * System.in：标准的输入流，默认从键盘输入
     * System.out：标准的输出流，默认从控制台输出
     */
    public static void main(String[] args) {
        InputStreamReader inputStreamReader = new InputStreamReader(System.in);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        try {
            while (true){
                System.out.println("请输入：");
                String data = bufferedReader.readLine();
                if("exit".equals(data)){
                    System.out.println("程序结束");
                    break;
                }
                System.out.println(data.toUpperCase());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(bufferedReader != null){
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
