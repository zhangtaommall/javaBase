package com.candy.IO流.字符流;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import org.junit.Test;

import java.io.*;

/**
 * @author Candy
 * @create 2020-08-31 15:51
 * 字节流                字符流
 * InputStream          Reader
 * OutputStream         Writer
 */
public class FileReaderTest {
    @Test
    public void Test(){
//        String str = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><response><result><errmsg>Success.</errmsg><code>0</code></result><image_url><![CDATA[https://192.168.2.130:8009/sdk_service/rest/video-analysis/alarm_pic?alarm_id=b6530e42fca311ea8000fa163ed8590d009270&inviteToken=87b3702b-e308-4420-9e4b-c29762506320]]></image_url></response>";
        String str = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "<content><common-info><task-id>159983575327115564</task-id><camera-id>08863410001316763553#62#2d57824d563b4231bf04f006b1682a2c</camera-id><camera-name>192_168_0_13</camera-name><camera-index>914</camera-index><vstation-sn/><vstation-name/><vstation-index>-2</vstation-index><case-id>-2</case-id><case-file-id/><slice-num/><source>1</source><source-system-id>4</source-system-id><resolution>352,288</resolution><alarm-id>cb0493d00f8411eb8000fa163ee3ff73004924</alarm-id><alarm-level>1</alarm-level><alarm-time>1602836165854</alarm-time><alarm-type>0</alarm-type><alarm-pic-name>cb0493d00f8411eb8000fa163ee3ff73004924.jpg</alarm-pic-name><ctime>1602834858111</ctime><rule-type>8</rule-type><confirm>1</confirm><closed>0</closed><domain-code>63f5c47ee2e24d1cb44e899b45f57aed</domain-code></common-info><private-info><meta-data db_name=\"vcm_ba_alarm_info\"><id type=\"string\" index=\"false\">47665</id><tp type=\"string\" index=\"false\">unknown</tp><track type=\"string\" index=\"false\">769,806;</track><crowd-density type=\"string\" index=\"false\">0</crowd-density></meta-data><pic><casefile-id>5f8951aa0b6d1674c8d69275</casefile-id><_fileId>/sre_storage/data1001/s2/omm_spc/vcm_behavior/20201016/07/dc8b5fd5-dc2f-4a12-ae13-5400f074b3e8</_fileId><_startPos>0</_startPos><_thumbLen>0</_thumbLen><_len>734149</_len><_name>cb0493d00f8411eb8000fa163ee3ff73004924.jpg</_name><_m_id>2026</_m_id><_s_inx>1000</_s_inx><_imageUrl/><_thumImageUrl/></pic></private-info></content>";
        JSONObject jsonObject = JSONUtil.parseFromXml(str);
        System.out.println(jsonObject);

    }


    /**
     * 数据读入获取：一个一个读入
     * 1.read()：返回读入的一个字符；如果达到文件末尾，返回-1
     * 2.close()：关闭流操作必须有
     */
    @Test
    public void FileReaderTest(){
        FileReader fileReader = null;
        try {
            File file = new File("ddd.txt");
            System.out.println(file.getAbsolutePath());

            fileReader = new FileReader(file);
            int read;
            while ((read = fileReader.read()) != -1){
                System.out.print((char) read);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //4.流的关闭操作(一定注意)
            try {
                if(fileReader != null){
                    fileReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 数据读入获取：批量读入操作
     *  read(char[] chars)：对于read()方法的一个升级,返回每次读入到数组中的字符个数
     */
    @Test
    public void FileReaderTest2(){
        FileReader fileReader = null;
        try {
            //1.File类的实例化
            File file = new File("ddd.txt");
            //2.FileReader类的实例化
            fileReader = new FileReader(file);
            //3.文件数据读入操作
            char[] chars = new char[20];
            int len;
            while ((len = fileReader.read(chars)) != -1){
                //写法一
//                for(int i = 0;i < len;i++){
//                    System.out.print(chars[i]);
//                }
                //写法二
                String str = new String(chars,0,len);
                System.out.print(str+"===");
                //错误写法(数组是修改操作，这样会把之前的数据打印出来)
//                for (char aChar : chars) {
//                    System.out.print(aChar);
//                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //4.资源关闭
            try {
                if(fileReader != null){
                    fileReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }


}
