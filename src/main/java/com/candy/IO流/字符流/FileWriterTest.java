package com.candy.IO流.字符流;

import org.junit.Test;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author Candy
 * @create 2020-08-31 17:21
 */
public class FileWriterTest {


    /**
     * 1.输出操作：对于的File文件可以不存在
     *      如果不存在：在输出过程中，会自动创建文件
     *      如果存在：根据构造器不同；
     *          FileWriter(file,true)不会覆盖之前的文件
     *          FileWriter(file,false)会覆盖之前的文件
     * 2.
     */
    @Test
    public void FileWriterTest(){
        //1.File类的实例化
        File file = new File("hello1.txt");
        FileWriter fileWriter = null;
        try {
            //2.FileWriter实例化
            fileWriter = new FileWriter(file,false);
            //3.文件数据的写出操作
            fileWriter.write("我今天很高兴,yeyeye!\n");
            fileWriter.write("oooo_oooo");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //4.资源关闭
            try {
                if(fileWriter != null){
                    fileWriter.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 结合使用：文件内容复制
     */
    @Test
    public void FileWriterAndReaderTest(){
        //1.File类的实例化
        File fileReader = new File("hello.txt");//读入文件
        File fileWriter = new File("hello1.txt");//写出文件
        FileReader fileReader1 = null;
        FileWriter fileWriter1 = null;
        try {
            //2.创建输入流和写出流的对象
            fileReader1 = new FileReader(fileReader);
            fileWriter1 = new FileWriter(fileWriter);
            //3.数据读入和写出的操作
            char[] chars = new char[6];
            int len;
            while ((len = fileReader1.read(chars)) != -1){
                fileWriter1.write(chars,0,len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //4.资源关闭
            try {
                if(fileWriter1 != null){
                    fileWriter1.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if(fileReader1 != null){
                    fileReader1.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
