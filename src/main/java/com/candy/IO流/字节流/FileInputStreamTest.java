package com.candy.IO流.字节流;

import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * @author Candy
 * @create 2020-09-01 13:40
 */
public class FileInputStreamTest {

    @Test
    public void fileInputStreamTest(){
        //1.File类的实例化
        File file = new File("hello.txt");
        FileInputStream inputStream = null;
        try {
            //2.FileInputStream实例化
            inputStream = new FileInputStream(file);
            //3.文件数据读入操作
            byte[] bytes = new byte[5];
            int len;
            while ((len = inputStream.read(bytes)) != -1){
                String str = new String(bytes,0,len);
                System.out.print(str);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //4.资源关闭
            try {
                if(inputStream != null){
                    inputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }

}
