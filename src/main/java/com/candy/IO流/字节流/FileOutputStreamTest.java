package com.candy.IO流.字节流;

import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author Candy
 * @create 2020-09-01 13:40
 */
public class FileOutputStreamTest {


    @Test
    public void fileOutputStreamTest(){
        //1.File类的实例化
//        File file1 = new File("C:\\Users\\61788\\Desktop\\01-视频.avi");
//        File file2 = new File("C:\\Users\\61788\\Desktop\\02-视频.avi");
        File file1 = new File("src\\main\\java\\com\\candy\\IO流\\字节流\\hello.txt");
        File file2 = new File("src\\main\\java\\com\\candy\\IO流\\字节流\\hello2.txt");
        FileInputStream inputStream = null;
        FileOutputStream outputStream = null;
        try {
            //2.FileInputStream和FileOutputStream实例化
            inputStream = new FileInputStream(file1);
            outputStream = new FileOutputStream(file2,false);
            //3.文件数据读入/写出操作
            byte[] bytes = new byte[1024];
            int len;
            while ((len = inputStream.read(bytes)) != -1){
                outputStream.write(bytes,0,len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //4.资源关闭
            try {
                if(outputStream != null){
                    outputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if(inputStream != null){
                    inputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
