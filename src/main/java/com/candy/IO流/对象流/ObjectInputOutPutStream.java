package com.candy.IO流.对象流;

import org.junit.Test;

import java.io.*;

/**
 * @author Candy
 * @create 2020-09-03 14:55
 * 对象字节流：用于序列化和反序列化
 * ObjectInputStream
 * ObjectOutputStream
 */
public class ObjectInputOutPutStream {

    /**
     * 序列化过程
     */
    @Test
    public void test(){
        ObjectOutputStream objectOutputStream = null;
        try {
            FileOutputStream fileInputStream = new FileOutputStream("object.txt");
            objectOutputStream = new ObjectOutputStream(fileInputStream);

            objectOutputStream.writeObject(new String("今天心情非常好"));
            objectOutputStream.flush();//一定要有刷新操作

            objectOutputStream.writeObject(new Person("孙健",31));
            objectOutputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(objectOutputStream != null){
                    objectOutputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 反序列化过程
     */
    @Test
    public void test2(){
        ObjectInputStream objectInputStream = null;
        try {
            FileInputStream fileInputStream = new FileInputStream("object.txt");
            objectInputStream = new ObjectInputStream(fileInputStream);

            String str = (String)objectInputStream.readObject();
            System.out.println(str);

            Person str2 = (Person)objectInputStream.readObject();
            System.out.println(str2);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if(objectInputStream != null){
                    objectInputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
