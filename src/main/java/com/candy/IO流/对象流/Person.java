package com.candy.IO流.对象流;

import java.io.Serializable;

/**
 * @author Candy
 * @create 2020-09-03 15:22
 */
public class Person implements Serializable {
    private static final long serialVersionUID = 2705782428976647835L;

    private String name;
    private Integer age;

    public Person(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    public Person() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
