package com.candy.IO流.缓冲流;

import org.junit.Test;

import java.io.*;

/**
 * @author Candy
 * @create 2020-09-02 10:09
 * 缓冲流：提高字符和字节流的传输速度(内部调用了flash方法，可以省略不写)
 * BufferedInputStream
 * BufferedOutputStream
 * BufferedWriter
 * BufferedReader
 */
public class BuffTest {

    /**
     * BufferedInputStream和BufferedOutputStream的使用
     */
    @Test
    public void BufferedInputStreamAndOutputStream(){
        //1.File类的实例化
        File file1 = new File("吃烧烤的小姐姐.jpg");
        File file2 = new File("吃烧烤的小姐姐2.jpg");
        FileInputStream fileInputStream = null;
        FileOutputStream fileOutputStream = null;
        BufferedInputStream bufferedInputStream = null;
        BufferedOutputStream bufferedOutputStream = null;
        try {
            //2.创建节点流
            fileInputStream = new FileInputStream(file1);
            fileOutputStream = new FileOutputStream(file2);
            //2.创建缓冲流BufferedInputStream实例化(包装InputStream的处理流)
            bufferedInputStream = new BufferedInputStream(fileInputStream);
            bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
            //3.文件数据读入操作
            byte[] bytes = new byte[1024];
            int len;
            while ((len = bufferedInputStream.read(bytes)) != -1){
                bufferedOutputStream.write(bytes,0,len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //4.资源关闭
            try {
                if(bufferedOutputStream != null){
                    bufferedOutputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if(bufferedInputStream != null){
                    bufferedInputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            //关闭处理流的同时，节点流会自动关闭
//            try {
//                if(fileOutputStream != null){
//                    fileOutputStream.close();
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            try {
//                if(fileInputStream != null){
//                    fileInputStream.close();
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
        }
    }


    @Test
    public void BufferedWriterAndReader(){
        //1.File类的实例化
        File file1 = new File("hello.txt");
        File file2 = new File("hello2.txt");
        BufferedReader bufferedReader = null;
        BufferedWriter bufferedWriter = null;
        try {
            //2.创建节点流
            FileReader fileReader = new FileReader(file1);
            FileWriter fileWriter = new FileWriter(file2,false);
            //3.创建缓存流
            bufferedReader = new BufferedReader(fileReader);
            bufferedWriter = new BufferedWriter(fileWriter);
            //4.文件数据读入操作(方式一)
//            char[] chars = new char[1024];
//            int len;
//            while ((len = bufferedReader.read(chars)) != -1){
//                bufferedWriter.write(chars,0,len);
//            }
            //方式二(文本会被压缩到一行显示)
            String str;
            while ((str = bufferedReader.readLine()) != null){
                bufferedWriter.write(str);
                //如果不想让文本压缩到一行需要加方法newLine()
                bufferedWriter.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //5.关闭资源
            try {
                if(bufferedWriter != null){
                    bufferedWriter.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if(bufferedReader != null){
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
