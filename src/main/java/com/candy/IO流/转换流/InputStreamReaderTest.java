package com.candy.IO流.转换流;

import org.junit.Test;

import java.io.*;

/**
 * @author Candy
 * @create 2020-09-02 16:02
 * 转换流：用于将字节流转换为字符流(属于字符的处理流)
 * InputStreamReader：将字节的输入流转换为字符的输入流
 * OutputStreamWriter：将字符的输出流转换为字节的输出流
 */
public class InputStreamReaderTest {

    @Test
    public void InputStreamReaderAndOutputStreamWriter(){
        InputStreamReader inputStreamReader = null;
        OutputStreamWriter outputStreamWriter = null;
        try {
            FileInputStream fileInputStream = new FileInputStream("hello.txt");
            FileOutputStream fileOutputStream = new FileOutputStream("hello_gbk.txt");
            //字符集必须要根据源文件自身的字符集为准(读取的时候必须和源文件字符集一致)
            inputStreamReader = new InputStreamReader(fileInputStream,"UTF-8");
            outputStreamWriter = new OutputStreamWriter(fileOutputStream,"gbk");
            char[] chars = new char[1024];
            int len;
            while ((len = inputStreamReader.read(chars)) != -1){
//                String str = new String(chars,0,len);
//                System.out.print(str);
                outputStreamWriter.write(chars,0,len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(outputStreamWriter != null){
                    outputStreamWriter.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if(inputStreamReader != null){
                    inputStreamReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
