package com.candy.JAVA8新特性.Lambda表达式;

import org.junit.Test;

import java.util.Comparator;

/**
 * @author Candy
 * @create 2020-10-20 17:17
 *  1.比如：(o1,o2) -> Integer.compare(o1,o2);
 *  2.格式：
 *          ->：lambda操作符
 *          ->左边：lambda形参列表（其实就是接口中抽象方法的形参列表）
 *          ->右边：lambda体(其实就是重新接口中的抽象方法的方法体)
 */
public class LamdbaTest {

    @Test
    public void test(){
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println("今天心情好");
            }
        };
        runnable.run();
        //使用Lamdba表达式后
        System.out.println("*************使用Lamdba表达式后***************");

        Runnable runnable2 = () -> System.out.println("我爱Lamdba表达式");

        runnable2.run();
    }
    @Test
    public void test2(){
        Comparator<Integer> comparator = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return Integer.compare(o1,o2);
            }
        };
        int compare = comparator.compare(12, 50);
        System.out.println(compare);
        //使用Lamdba表达式后
        System.out.println("*************使用Lamdba表达式后***************");
        Comparator<Integer> comparator2 = (o1,o2) -> Integer.compare(o1,o2);
        int compare1 = comparator2.compare(12, 5);
        System.out.println(compare1);
        //使用方法引用后
        System.out.println("*************使用方法引用后***************");
        Comparator<Integer> comparator3 = Integer :: compare;
        int compare2 = comparator3.compare(12, 5);
        System.out.println(compare2);

    }

}
