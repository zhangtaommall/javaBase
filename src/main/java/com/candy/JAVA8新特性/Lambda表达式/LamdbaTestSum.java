package com.candy.JAVA8新特性.Lambda表达式;

import org.junit.Test;

import java.util.Comparator;
import java.util.function.Consumer;

/**
 * @author Candy
 * @create 2020-10-27 11:08
 */
public class LamdbaTestSum {

    //第一种表达形式
    @Test
    public void test(){
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println("今天心情好");
            }
        };
        runnable.run();
        //使用Lamdba表达式后
        System.out.println("*************使用Lamdba表达式后***************");

        Runnable runnable2 = () -> {System.out.println("我爱Lamdba表达式");};

        runnable2.run();
    }

    //第二种表达形式：需要参数，但是没有返回值
    @Test
    public void test2(){
        Consumer<String> consumer = new Consumer<String>() {
            @Override
            public void accept(String s) {
                System.out.println(s);
            }
        };
        consumer.accept("Lambda第二种表达形式");

        System.out.println("*************使用Lamdba表达式后***************");

        Consumer<String> consumer2 = (String s) -> {
            System.out.println(s);
        };
        consumer2.accept("使用Lamdba表达式");
    }

    //第三种表达形式：数据类型省略，因为编译器可以推断得出，称为"类型推断"
//                  如果参数只需要一个的时候，参数的小括号也可以省略
    @Test
    public void test3(){
        //可以根据泛型做类型推断
        Consumer<String> consumer2 = (String s) -> {
            System.out.println(s);
        };
        consumer2.accept("使用Lamdba表达式");

        System.out.println("*************使用Lamdba表达式'类型推断'优化后***************");

        Consumer<String> consumer3 = s -> {
            System.out.println(s);
        };
        consumer3.accept("使用Lamdba表达式'类型推断'");
    }

    /**
     * 第四种表达形式：需要两个或以上的参数，多条执行语句，并且有返回值
     */
    @Test
    public void test4(){
        Comparator<Integer> comparator = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                System.out.println(o1);
                return o1.compareTo(o2);
            }
        };
        comparator.compare(12,22);
        System.out.println("*************使用Lamdba表达式后***************");

        Comparator<Integer> comparator2 = (o1,o2) -> {
            System.out.println(o1);
            return o1.compareTo(o2);
        };
        comparator2.compare(12,6);
    }

    /**
     * 第五种表达形式：当lambda体只有一条语句时，return与大括号都可以省略
     */
    @Test
    public void test5(){
        Comparator<Integer> comparator = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1.compareTo(o2);
            }
        };
        comparator.compare(12,22);
        System.out.println("*************使用Lamdba表达式后***************");

        Comparator<Integer> comparator2 = (o1, o2) -> o1.compareTo(o2);

    }

}
