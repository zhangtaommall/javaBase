package com.candy.JAVA8新特性.StreamAPI;

import com.candy.JAVA8新特性.Employee;
import com.candy.JAVA8新特性.EmployeeData;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * @author Candy
 * @create 2020-12-02 15:35
 */
public class StreamAPITest {


    //Stream实例化方式一：通过集合
    @Test
    public void test(){
        List<Employee> employees = EmployeeData.getEmployees();
        Stream<Employee> stream = employees.stream();

        Stream<Employee> employeeStream = employees.parallelStream();
    }

    //Stream实例化方式二：通过数组
    @Test
    public void test2(){
        int[] ints = new int[]{1,2,3,4,5,6,7};
        IntStream stream = Arrays.stream(ints);

        Employee employee = new Employee(1002,"Candy");
        Employee employee2 = new Employee(1003,"Tom");
        Employee[] employees = new Employee[]{employee,employee2};
        Stream<Employee> stream1 = Arrays.stream(employees);
    }
    //Stream实例化方式二：通过Stream类
    @Test
    public void test3(){
        Stream<Integer> integerStream = Stream.of(1, 2, 3, 4, 5);
    }

}
