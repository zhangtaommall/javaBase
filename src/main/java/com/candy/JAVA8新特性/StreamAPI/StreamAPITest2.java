package com.candy.JAVA8新特性.StreamAPI;

import com.candy.JAVA8新特性.Employee;
import com.candy.JAVA8新特性.EmployeeData;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Candy
 * @create 2020-12-02 15:35
 * 筛选与过滤
 */
public class StreamAPITest2 {


    //Stream实例化方式一：通过集合
    @Test
    public void test(){
        List<Employee> list = new ArrayList<>();
        List<Employee> employeeList = EmployeeData.getEmployees();
//        filter(Predicate p) 接收 Lambda ， 从流中排除某些元素
        Stream<Employee> stream = employeeList.stream();
        //查询年龄大于40的
        stream.filter(employee -> employee.getAge() > 40).forEach(list::add);
//        limit(long maxSize) 截断流，使其元素不超过给定数量
        System.out.println();
        employeeList.stream().limit(3).forEach(System.out::println);
//        skip(long n)
//        跳过元素，返回一个扔掉了前 n 个元素的流。若流中元素不足 n 个，则返回一
//        个空流。与 limit(n) 互补
        System.out.println();
        employeeList.stream().skip(3).forEach(System.out::println);
        //        distinct() 筛选，通过流所生成元素的 hashCode() 和 equals() 去除重复元素
        employeeList.add(new Employee(1003, "刘强东", 33, 8000));
        employeeList.add(new Employee(1003, "刘强东", 33, 3000.82));
        employeeList.add(new Employee(1003, "刘强东", 33, 3000.82));
        employeeList.add(new Employee(1003, "刘强东", 38, 3000.82));
        System.out.println();
        employeeList.stream().distinct().forEach(System.out::println);
    }

    //根据两个属性来去重
    @Test
    public void test2(){
        List<Employee> employeeList = new ArrayList<>();
        employeeList.add(new Employee(1003, "刘强东", 33, 8000));
        employeeList.add(new Employee(1003, "刘强东", 31, 3000.82));
        employeeList.add(new Employee(1003, "刘强东", 22, 3000.82));
        employeeList.add(new Employee(1003, "刘强东1", 12, 3000.82));
        ArrayList<Employee> collect = employeeList.stream()
                .collect(Collectors.collectingAndThen(Collectors.toCollection(() ->
                                new TreeSet<>(Comparator.comparing(f -> f.getId() + f.getName()))),
                        ArrayList::new));

        System.out.println(collect);
    }

    @Test
    public void test3(){
        List<Employee> employeeList = EmployeeData.getEmployees();
//        List<Employee> employeeList2 = EmployeeData.getEmployees();
//        employeeList2.add(new Employee(1003, "刘强东1", 12, 3000.82));
        List<Employee> employeeList2 = new ArrayList<>();
        employeeList2.add(new Employee(1001, "张凯", 30, 8000));
        employeeList2.add(new Employee(1002, "孙健", 31, 3000.82));
        employeeList2.add(new Employee(1003, "拉拉", 22, 3000.82));
        employeeList2.add(new Employee(1004, "马化腾", 34, 3000.82));
        for (Employee employee : employeeList2) {
            if(employeeList.stream().filter(e -> e.getName() == employee.getName())
                    .filter(e -> e.getAge() == employee.getAge())
                    .findAny().isPresent()){
                //拿到当前对象
                Employee atPresentEmployee = employeeList.stream().filter(e -> e.getName() == employee.getName())
                        .filter(e -> e.getAge() == employee.getAge())
                        .findAny().get();
                System.out.println("存在-->"+employee.getName());
            }else{
                System.out.println("不存在-->"+employee.getName());
            }
        }

    }

}
