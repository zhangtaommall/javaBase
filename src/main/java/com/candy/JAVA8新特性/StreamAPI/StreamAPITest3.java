package com.candy.JAVA8新特性.StreamAPI;

import com.candy.JAVA8新特性.Employee;
import com.candy.JAVA8新特性.EmployeeData;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * @author Candy
 * @create 2020-12-02 15:35
 * 中间操作：映射，排序;
 * 终止操作：匹配和查找，归约;
 */
public class StreamAPITest3 {

    @Test
    public void test(){
        List<Employee> employeeList = EmployeeData.getEmployees();
        employeeList.stream().map(str -> str.getName()+"XXX").forEach(System.out::println);
        //过滤filter获取name长度大于3的数据
        employeeList.stream().map(Employee :: getName).filter(name -> name.length() > 3).forEach(System.out::println);
        //map()和flatMap()两个的区别就是add()和addAll()
        employeeList.stream().map(str -> str.getName()).filter(name -> name.length() > 3)
                .flatMap(StreamAPITest3 :: formString).forEach(System.out::println);
    }

    public static Stream<Character> formString(String str){
        ArrayList<Character> characters = new ArrayList<>();
        for (Character character : str.toCharArray()) {
            characters.add(character);
        }
        return characters.stream();
    }

    //排序
    @Test
    public void test2(){
        List<Integer> integers = Arrays.asList(12, 55, 87, 1, 586, 5, 45, 88, -99, 1546, 9);
        integers.stream().sorted().forEach(System.out::println);

        //按照年龄排序
        List<Employee> employees = EmployeeData.getEmployees();
        employees.stream().sorted((e1,e2) -> Integer.compare(e1.getAge(),e2.getAge())).forEach(System.out::println);
    }

    //匹配和查找
    @Test
    public void test3(){
        List<Employee> list = new ArrayList<>();
        List<Employee> employees = EmployeeData.getEmployees();
        employees.stream().filter(e -> e.getSalary() > 5000).forEach(list::add);
//        allMatch(Predicate p) 检查是否匹配所有元素      返回boolean
        //是否所有员工都大于18岁
        boolean b = employees.stream().allMatch(e -> e.getAge() > 18);
        System.out.println(b);
//        anyMatch(Predicate p) 检查是否至少匹配一个元素
        //是否存在某个员工工资大于10000     返回boolean
        boolean b1 = employees.stream().anyMatch(e -> e.getSalary() >= 10000);
        System.out.println(b1);
//        noneMatch(Predicate p) 检查是否没有匹配所有元素
        //是否存在员工的姓"张"
        boolean b3 = employees.stream().noneMatch(e -> e.getName().startsWith("张"));
        System.out.println(b3);
//        findFirst() 返回第一个元素
        Optional<Employee> first = employees.stream().findFirst();
        System.out.println(first);
//        findAny() 返回当前流中的任意元素
        Optional<Employee> any = employees.stream().findAny();
        System.out.println(any);
//        count() 返回流中元素总数
//        max(Comparator c) 返回流中最大值
        Optional<Double> max = employees.stream().map(e -> e.getSalary()).max(Double::compare);
        System.out.println(max);
//        min(Comparator c) 返回流中最小值
        Optional<Employee> min = employees.stream().min((e1, e2) -> Double.compare(e1.getSalary(), e2.getSalary()));
        System.out.println(min);
//        forEach(Consumer c)
//        内部迭代(使用 Collection 接口需要用户去做迭代，
//                称为外部迭代。相反，Stream API 使用内部迭
//                代——它帮你把迭代做了)
    }

}
