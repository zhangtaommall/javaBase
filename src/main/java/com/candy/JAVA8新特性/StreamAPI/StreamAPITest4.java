package com.candy.JAVA8新特性.StreamAPI;

import com.candy.JAVA8新特性.Employee;
import com.candy.JAVA8新特性.EmployeeData;
import org.junit.Test;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Candy
 * @create 2020-12-09 17:33
 * 终止操作：归约，收集;
 */
public class StreamAPITest4 {

    //归约(求和)
    @Test
    public void test(){
        List<Employee> employees = EmployeeData.getEmployees();
//        reduce(T iden, BinaryOperator b):(初始值iden) 可以将流中元素反复结合起来，得到一
//        个值。返回 T
        Double reduce = employees.stream().map(e -> e.getSalary()).reduce(0.0, Double::sum);
        System.out.println(reduce);
//        reduce(BinaryOperator b) 可以将流中元素反复结合起来，得到一
//        个值。返回 Optional<T>
        Optional<Double> reduce1 = employees.stream().map(Employee::getSalary).reduce((d1,d2) -> d1+d2);
        System.out.println(reduce1);
    }

    //收集(例如收集到List,Set,Map)
    @Test
    public void test2(){
        List<Employee> employees = EmployeeData.getEmployees();
        //查询工资大于6000的员工，结果返回一个新的List
        List<Employee> collect = employees.stream().filter(e -> e.getSalary() > 6000).collect(Collectors.toList());
        collect.forEach(System.out::println);
        System.out.println("***********************************");
        Set<Employee> employeeSet = employees.stream().filter(e -> e.getSalary() > 6000).collect(Collectors.toSet());
        employeeSet.forEach(System.out::println);
    }


}
