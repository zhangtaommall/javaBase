package com.candy.JAVA8新特性.函数式interface;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * @author Candy
 * @create 2020-12-02 10:39
 * Java内置四大核心函数式接口
 * 1.消费型接口：Consumer<T>      void accept(T t)
 * 2.供给型接口：Supplier<T>      T get()
 * 3.函数型接口：Function<T,R>    R apply(T t)
 * 4.断定型接口：Predicate<T>     boolen test(T t)
 */
public class LambdaTest {


    @Test
    public void test(){
        happyTime(354.22, new Consumer<Double>() {
            @Override
            public void accept(Double aDouble) {
                System.out.println("今天天气很好"+aDouble);
            }
        });
        System.out.println("****************************");
        happyTime(444.44,aDouble -> System.out.println("今天天气很好"+aDouble));
    }

    public void happyTime(double money, Consumer<Double> con){
        con.accept(money);
    }
    @Test
    public void test2(){
        List<String> list = Arrays.asList("北京","南京","天津","东京","西京","普京");

        List<String> strings = filterString(list, new Predicate<String>() {
            @Override
            public boolean test(String s) {
                return s.contains("京");
            }
        });
        System.out.println(strings);

        List<String> stringList = filterString(list, s -> s.contains("津"));
        System.out.println(stringList);
    }

    //根据Predicate接口中指定的规则过滤字符串
    public List<String> filterString(List<String> list, Predicate<String> pre){

        List<String> filStringList = new ArrayList<>();
        for (String s : list) {
            if(pre.test(s)){
                filStringList.add(s);
            }
        }
        return filStringList;
    }

}
