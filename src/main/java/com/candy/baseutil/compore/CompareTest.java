package com.candy.baseutil.compore;

import java.util.Arrays;
import java.util.Comparator;

/**
 * @author Candy
 * @create 2020-07-03 11:09
 * 比较器
 */
public class CompareTest {
    public static void main(String[] args) {
        Person[] peoples = new Person[4];
        peoples[0] = new Person("张凯",27);
        peoples[1] = new Person("范瑞",31);
        peoples[2] = new Person("孙健",28);
        peoples[3] = new Person("张涛",25);

//        Arrays.sort(peoples);
//        System.out.println(Arrays.toString(peoples));


        /**
         * 如果实体类没有实现Comparable接口，或者不方便修改代码，
         * 或者之前写的排序不符合现在的要求
         * 使用定制排序：实现Comparator
         *
         * 重写compare(T o1, T o2)方法
         * 返回正数：o1>02
         * 返回0：相等
         * 返回负数：o1<02
         */
        Arrays.sort(peoples,new Comparator<Person>(){
            //先按照姓名从小到大，在按照岁数从高到低
            @Override
            public int compare(Person o1, Person o2) {
//                return o1.getAge()-o2.getAge();//从小到大排序
//                return -(o1.getAge()-o2.getAge());//从大到小排序
                if(o1.getName().equals(o2.getName())){
                    return -(o1.getAge()-o2.getAge());
                }else{
                    return o1.getName().compareTo(o2.getName());
                }
            }
        });
        System.out.println(Arrays.toString(peoples));

    }

}
