package com.candy.baseutil.compore;

/**
 * @author Candy
 * @create 2020-07-06 13:34
 * String,包装类都写好了compareTo()方法，只有自定义的类才需要手动写compareTo(方法)
 * 实现接口Comparable表示自然排序
 */
public class Person implements Comparable{

    private String name;
    private Integer age;

    public Person() {}

    public Person(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public int compareTo(Object o) {
        if(o instanceof Person){
            Person person = (Person) o;
            return this.age - person.age;//从小到大排序
//            return -(this.age - person.age);//从大到小排序
        }
        throw new RuntimeException("数据类型不匹配");
    }
}
