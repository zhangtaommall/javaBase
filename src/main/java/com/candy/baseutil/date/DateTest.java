package com.candy.baseutil.date;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;

/**
 * @author Candy
 * @create 2020-07-02 17:27
 * JDK8的时间API
 */
public class DateTest {
    public static void main(String[] args) {
        /**
         * LocalDate、LocalTime、LocalDateTime
         * now()获取当前时间
         */
        LocalDate localDate = LocalDate.now();
        System.out.println(localDate);
        LocalTime localTime = LocalTime.now();
        System.out.println(localTime);
        LocalDateTime localDateTime = LocalDateTime.now();
        System.out.println(localDateTime);
        //of方法,设置年月日时分秒,不会有偏移量
        LocalDateTime of = LocalDateTime.of(2018, 5, 2, 20, 21, 58);
        System.out.println(of);
        LocalTime of1 = LocalTime.of(15, 22, 55);
        System.out.println(of1);
        LocalDate of2 = LocalDate.of(2019, 4, 22);
        System.out.println(of2);
        //getXXX()
        System.out.println(localDateTime.getYear());
        System.out.println(localDateTime.getMonthValue());
        System.out.println(localDateTime.getDayOfMonth());
        /**
         * 时期格式化DateTimeFormatter
         */
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String format = formatter.format(localDateTime);
        System.out.println(format);
        TemporalAccessor parse = formatter.parse(format);
        System.out.println(parse);

    }
}
