package com.candy.baseutil.myenum;

/**
 * @author Candy
 * @create 2020-07-06 17:10
 */
public class enumTest {
    public static void main(String[] args) {
        Candy one = Candy.ONE;
        System.out.println(one);

        Candy[] values = Candy.values();
        for (int i = 0; i < values.length; i++) {
            System.out.println(values[i].getStatus());
        }
    }

}
enum Candy{

//    public static final Candy a = new Candy(100,"一");
    ONE(100,"一"),
    TWO(200,"二"),
    THREE(300,"三"),
    FOUR(4100,"四");

    private final Integer status;
    private final String statusName;

    Candy(Integer status, String statusName) {
        this.status = status;
        this.statusName = statusName;
    }

    public Integer getStatus() {
        return status;
    }

    public String getStatusName() {
        return statusName;
    }
}
