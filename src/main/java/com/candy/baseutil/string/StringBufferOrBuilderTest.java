package com.candy.baseutil.string;

import java.util.Arrays;

/**
 * @author Candy
 * @create 2020-07-01 16:08
 */
public class StringBufferOrBuilderTest {
    public static void main(String[] args) {
        /*
            String,StringBuffer,StringBuilder的异同,那个效率高？为什么？
            String：不可变的字符序列,char[]数组存储数据
            StringBuffer：可变的字符序列，线程安全的，效率低 ,char[]数组存储数据
            StringBuilder：可变的字符序列，线程不安全，效率高 ,char[]数组存储数据
            效率：StringBuilder>StringBuffer>String(Buf和Builder差距在synchronized关键字降低效率)
         */
        /*
            StringBuffer append(xxx)：提供了很多的append()方法，用于进行字符串拼接
            StringBuffer delete(int start,int end)：删除指定位置的内容
            StringBuffer replace(int start, int end, String str)：把[start,end)位置替换为str
            StringBuffer insert(int offset, xxx)：在指定位置插入xxx
            StringBuffer reverse() ：把当前字符序列逆
         */
//        String s = null;
//        StringBuffer buffer = new StringBuffer();
//        buffer.append(s);
//        System.out.println(buffer);
//
//        String a = "a";
//        a = "b";
//        System.out.println(a);


    }
}
