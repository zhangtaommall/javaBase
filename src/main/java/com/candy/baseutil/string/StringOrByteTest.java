package com.candy.baseutil.string;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

/**
 * @author Candy
 * @create 2020-06-30 15:20
 */
public class StringOrByteTest {

    public static void main(String[] args) {
        //String与bute之间的转换
        String s1 = "abc123中国";
        byte[] bytes = s1.getBytes();//使用默认的编码集
        System.out.println(Arrays.toString(bytes));

        //指定编码格式
        try {
            byte[] gbks = s1.getBytes("gbk");
            System.out.println(Arrays.toString(gbks));
            String s2 = new String(bytes);
            System.out.println(s2);
            //只有编码集和解码集一致才不会乱码
            String s3 = new String(gbks,"gbk");
            System.out.println(s3);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


    }
}
