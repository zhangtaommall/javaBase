package com.candy.baseutil.string;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Candy
 * @create 2020-06-24 16:14
 */
public class StringTest {

    private static int counter = 0;

    public static void main(String[] args) {
//        String s = "a";
//        final String s2 = "a";
//        String s3 = "ab";
//        String s4 = s+"b";
//        String s5 = s.intern()+"b";
//        String s6 = new String("a");
//        String s7 = new String("ab");
//        String s8 = s2+"b";
//        System.out.println(s == s6);//f
//        System.out.println(s3 == s4);//f
//        System.out.println(s3 == s5);//f
//        System.out.println(s3 == s7);//f
//        System.out.println(s7 == s6);//f
//        System.out.println(s8 == s3);//t
//        StringBuffer buffer = new StringBuffer("a");
//        String s1 = "a";
//        long l = System.currentTimeMillis();
//        for (int i = 0; i < 100000; i++) {
//            s1 += "b";
//        }
//        long l1 = System.currentTimeMillis();
//        System.out.println(l1-l);
//        for (int i = 0; i < 100000; i++) {
//            buffer.append("b");
//        }
//        System.out.println(System.currentTimeMillis() - l1);

        //已知字符串如下，请返回里面有几个字符串"ab"
        //如果能，请抽象出此方法
//        String s1 = "fwefewfabfewgfwefaboyhkop[hoab[oeyjkotabgrehtyab";
//        System.out.println(countStr(s1,"f"));

        //前2个，后三个字符顺序不变，中间的反转
//        String s2 = "abcde12345";
//        String s = soutStr2(s2, 5, 1);
//        System.out.println(s);
    }

    //字符串倒序排列，可以指定前后不变的字母
    private static String soutStr2(String str,int before,int end){
        StringBuffer original = new StringBuffer(str);//原本的字符串
        String substring = str.substring(before, original.length() - end);//取出需要倒序的字符串
        StringBuffer reverse = new StringBuffer(substring).reverse();//倒序方法
        original.replace(before,original.length()-end,reverse.toString());//替换掉需要倒序的字符串
        return original.toString();
    }
    //字符串倒序排列，可以指定前后不变的字母
    private static String soutStr(String str,int before,int end){
        StringBuffer buffer = new StringBuffer();
        char[] chars = str.toCharArray();
        List<Character> beforeChar = new ArrayList<>();
        List<Character> endChar = new ArrayList<>();
        for (int i = 0; i <= chars.length-1; i++) {
            if(i >= before && i <= chars.length-1-end){
                buffer.append(chars[i]);
            }else{
                if(i >= before){
                    endChar.add(chars[i]);
                }else{
                    beforeChar.add(chars[i]);
                }
            }
        }
        char[] chars2 = buffer.toString().toCharArray();
        for (int i = 0; i <= (chars2.length - 2)/2; i++) {
            char temp = chars2[i];
            chars2[i] = chars2[chars2.length -i -1];
            chars2[chars2.length -i -1] = temp;
        }
        String beforeStr = "";
        for (Character character : beforeChar) {
            beforeStr += character;
        }
        String endStr = "";
        for (Character character : endChar) {
            endStr += character;
        }
        StringBuffer buffer1 = new StringBuffer(new String(chars2));
        buffer1.insert(0,beforeStr);
        buffer1.append(endStr);
        return buffer1.toString();
    }

    private static int countStr(String originally,String str){
        int num = 0;
        int num2 = 0;
        while (true){
            num = originally.indexOf(str, num);
            if(num >= 0){
                num += str.length();
                num2++;
            }else{
                break;
            }
        }
        return num2;
    }
}
