package com.candy.baseutil.util;


import org.junit.Test;

import java.math.BigDecimal;

/**
 * 1.System
 * 2.Math
 * 3.BigDecimal和BigInteger
 * @author Candy
 * @create 2020-07-06 15:06
 */
public class OtherClassTest {

    @Test
    public void test1(){
        System.out.println(System.getProperty("java.version"));
        System.out.println(System.getProperty("java.home"));
        System.out.println(System.getProperty("os.name"));
        System.out.println(System.getProperty("os.version"));
    }

    @Test
    public void test2(){
        BigDecimal b1 = new BigDecimal("8854815");
        BigDecimal b2 = new BigDecimal("12345.351");
        BigDecimal b3 = new BigDecimal("11");
        System.out.println(b2.divide(b3,BigDecimal.ROUND_HALF_UP));//BigDecimal.ROUND_HALF_UP表示四舍五入，默认保留3位
        System.out.println(b2.divide(b3,10,BigDecimal.ROUND_HALF_UP));//指定保留15位


    }
}
