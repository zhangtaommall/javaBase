package com.candy.baseutil.注解;

import java.lang.annotation.*;

/**
 * 自定义注解
 *  元注解：
 *  1.@Retention：注解的作用范围(生命周期),只有声明为RUNTIME作用域最大
 *  2.@Target：注解能标注在那些类型上面(类，属性，方法，构造器等等)
 *  3.@Documented：注解可保留在javadoc里面
 *  4.@Inherited：使用了带有@Inherited的注解，其子类都会被修饰
 * @author Candy
 * @create 2020-07-28 10:15
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE,ElementType.FIELD,ElementType.METHOD,ElementType.PARAMETER})
public @interface MyAnnoation {

    String value() default "Candy";
    String[] args() default {"Candy","dsdsd"};

}
