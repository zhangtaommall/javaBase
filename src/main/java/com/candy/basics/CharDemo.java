package com.candy.basics;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Candy
 * @create 2019-11-05 10:07
 * 字符char和转义符'\'的使用
 * 不可以存空字符 ''
 */
public class CharDemo {
    public static void main(String[] args) {
//        char c = '';//编译不通过
        char s = '\u0043';//可以直接使用Unicode编码
        char s1 = '\t';//可以使用转义符
        System.out.println(s);
        System.out.println(s1);
        String str = "dada\\ns\"你和\"asa";//可以使用转义符'\'输出\n
        System.out.println(str);
    }

}
