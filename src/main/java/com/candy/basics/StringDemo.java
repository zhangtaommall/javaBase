package com.candy.basics;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;

/**
 * @author Candy
 * @create 2019-11-05 17:58
 * 1.String属于引用数据类型
 * 二进制(binary)：0,1 ，满2进1.以0b或0B开头。
 * 十进制(decimal)：0-9 ，满10进1。
 * 八进制(octal)：0-7 ，满8进1. 以数字0开头表示。
 * 十六进制(hex)：0-9及A-F，满16进1. 以0x或0X开头表示。
 * 此处的A-F不区分大小写。 如：0x21AF +1= 0X21B0
 *
 * 原码：直接将一个数值换成二进制数。最高位是符号位
 * 负数的反码：是对原码按位取反，只是最高位（符号位）确定为1。
 * 负数的补码：其反码加1(计算机底层都以补码的方式存储数据)
 */
public class StringDemo {
    public static void main(String[] args) {
        int num = 0B1111;
        System.out.println(num);
    }
}
