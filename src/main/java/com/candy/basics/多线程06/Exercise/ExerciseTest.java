package com.candy.basics.多线程06.Exercise;

/**
 * @author Candy
 * @create 2020-06-23 11:53
 * 生产者(Productor)将产品交给店员(Clerk)，而消费者(Customer)从店员处 取走产品，
 * 店员一次只能持有固定数量的产品(比如:20），如果生产者试图 生产更多的产品，店员会叫生产者停一下，
 * 如果店中有空位放产品了再通 知生产者继续生产；如果店中没有产品了，店员会告诉消费者等一下，
 * 如 果店中有产品了再通知消费者来取走产品
 */
public class ExerciseTest {
    public static void main(String[] args) {
        Clerk clerk = new Clerk();
        Productor productor1 = new Productor(clerk);

        Customer customer1 = new Customer(clerk);
        Customer customer2 = new Customer(clerk);

        Thread thread1 = new Thread(productor1);
        Thread thread2 = new Thread(customer1);
        Thread thread3 = new Thread(customer2);
        thread1.setName("生产者1");
        thread2.setName("消费者1");
        thread3.setName("消费者2");
        thread1.start();
        thread2.start();
        thread3.start();
    }

}

class Productor implements Runnable{//生产者

    private Clerk clerk;

    public Productor(Clerk clerk){
        this.clerk = clerk;
    }
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName()+"：开始生产....");
        while (true){
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            clerk.chanping();
        }
    }

}

class Clerk{//店员

    private int chanP;

    public synchronized void chanping() {
        if(chanP < 20){
            chanP++;
            System.out.println(Thread.currentThread().getName()+"：开始生产第"+chanP+"个产品");
            notify();
        }else{
            //如果多了，需要让线程阻塞
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    public synchronized void xiaofei() {
        if(chanP > 0){
            System.out.println(Thread.currentThread().getName()+"：开始消费第"+chanP+"个产品");
            chanP--;
            notify();
        }else{
            //如果小于等于0个需要让线程阻塞
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
class Customer implements Runnable{//消费者

    private Clerk clerk;

    public Customer(Clerk clerk){
        this.clerk = clerk;
    }
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName()+"：开始消费....");
        while (true){
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            clerk.xiaofei();
        }
    }
}
