package com.candy.basics.多线程06;

/**
 * @author Candy
 * @create 2020-06-12 10:58
 * 多线程的创建方式一：继承Thread类，重写run方法
 * 如果想创建多个线程，则需要创建多个Thread子类对象(ThreadTest)
 * Thread.currentThread().getName()：返回线程名字
 * Thread.currentThread().setName("XXX")：设置线程名字
 * yield()：取消当前线程的执行权(线程让步)
 * join()：阻塞当前线程;在线程a中调用线程b的join(),此时线程a进入阻塞状态,直到线程b全部执行完成才结束a的阻塞状态.
 * sleep(2000)：线程休息;2000毫秒表示2秒
 * isAlive()：判断线程是否存活
 *
 * 线程的优先级
 * MIN_PRIORITY = 1;最小优先级
 * MAX_PRIORITY = 10;最大优先级
 * NORM_PRIORITY = 5;默认优先级
 * getPriority()：获取当前线程的优先级
 * setPriority(int p)：设置当前线程的优先级
 */
public class ThreadTest extends Thread{

    public static void main(String[] args) {
//        try {
//            Thread.sleep(2000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        ThreadTest threadTest = new ThreadTest();
        threadTest.setName("zt");
        threadTest.setPriority(Thread.MAX_PRIORITY);
        threadTest.start();
        ThreadTest threadTest2 = new ThreadTest(){
            @Override
            public void run() {
                for (int i=0;i<100;i++){
                    if(i % 2 != 0){
                        System.out.println("线程名称："+Thread.currentThread().getName()+"==线程优先级："+Thread.currentThread().getPriority()+"=="+i);
                    }
//                    if(i == 11){
//                        try {
//                            threadTest.join();
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }
//                    }
                }
                System.out.println(threadTest.isAlive());
            }
        };
        threadTest2.start();

    }

    @Override
    public void run() {
        for (int i=0;i<100;i++){
            if(i % 2 == 0){
                System.out.println(Thread.currentThread().getName()+"-----"+i);
            }
        }
    }
}
