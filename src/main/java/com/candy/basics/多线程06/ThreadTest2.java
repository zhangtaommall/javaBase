package com.candy.basics.多线程06;

/**
 * @author Candy
 * @create 2020-06-12 15:51
 * 多线程的创建方式二：实现接口Runnable,重写run方法，
 * 将实现了Runnable接口的类传入到Thread中，调用start()方法
 */
public class ThreadTest2 implements Runnable{

    @Override
    public void run() {
        for (int i=0;i<100;i++){
            if(i % 2 == 0){
                System.out.println(Thread.currentThread().getName()+"-----"+i);
            }
        }
    }

    public static void main(String[] args) {
        ThreadTest2 test2 = new ThreadTest2();
        Thread thread = new Thread(new ThreadTest2(){
            @Override
            public void run() {
                for (int i=0;i<100;i++){
                    if(i % 2 != 0){
                        System.out.println(Thread.currentThread().getName()+"-----"+i);
                    }
                }
            }
        });
        thread.start();
    }
}
