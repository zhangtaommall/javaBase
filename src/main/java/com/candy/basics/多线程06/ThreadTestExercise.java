package com.candy.basics.多线程06;

/**
 * @author Candy
 * @create 2020-06-12 15:32
 * 创建三个窗口买100张票,此时如果选择使用继承Thread类的方式会出现线程安全问题.如下
 *
 * 解决线程安全问题：
 *
 * 方式一：同步代码块
 * synchronized(同步监视器){
 *  //需要被同步的代码
 * }
 * 同步监视器：必须共用一个(任何一个类的对象，都可以做为锁)
 * 多个线程必须公用一个锁
 * 劣势就是效率变低了
 *
 * 方式二：
 *
 *
 *
 */
public class ThreadTestExercise extends Thread{

    private static int piao = 100;//必须公用一个成员变量，必须加static
    private String name;

    public ThreadTestExercise(String name){
        this.name = name;
    }

    @Override
    public void run() {
        setName(name);
        while (true){
            synchronized (ThreadTestExercise.class){
                if(piao > 0){
//                try {
//                    Thread.sleep(100);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
                    System.out.println("线程名称："+getName()+"余票："+piao);
                    piao--;
                }else{
                    break;
                }
            }
        }
    }

    public static void main(String[] args) {
        ThreadTestExercise threadTest = new ThreadTestExercise("窗口一");
        threadTest.start();
        ThreadTestExercise threadTestExercise = new ThreadTestExercise("窗口二");
        threadTestExercise.start();
        ThreadTestExercise threadTest3 = new ThreadTestExercise("窗口三");
        threadTest3.start();
    }
}
