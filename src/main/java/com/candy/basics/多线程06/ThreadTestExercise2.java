package com.candy.basics.多线程06;

/**
 * @author Candy
 * @create 2020-06-12 15:32
 * 创建三个窗口买100张票,此时如果选择使用继承Thread类的方式会出现线程安全问题.如下
 *
 * 解决线程安全问题：
 *
 * 方式一：同步代码块
 * synchronized(同步监视器){
 *  //需要被同步的代码
 * }
 * 同步监视器：必须共用一个(任何一个类的对象，都可以做为锁)
 * 多个线程必须公用一个锁
 * 劣势就是效率变低了
 *
 * 方式二：同步方法(同步方法的默认锁是this当前对象)
 * 静态同步方法的默认锁是:XXX.class
 * 同步方法是直接在方法上加synchronized关键字
 */
public class ThreadTestExercise2 implements Runnable{

    private int piao = 100;//必须公用一个成员变量，必须加static
    @Override
    public void run() {
        while (true){
            synchronized (ThreadTestExercise2.class){
                if(piao > 0){
                    System.out.println("线程名称："+Thread.currentThread().getName()+"余票："+piao);
                    piao--;
                }else{
                    break;
                }
            }
        }
    }

    public static void main(String[] args) {
        ThreadTestExercise2 threadTest = new ThreadTestExercise2();
        Thread thread = new Thread(threadTest);
        thread.setName("窗口一");
        Thread thread2 = new Thread(threadTest);
        thread.setName("窗口二");
        Thread thread3 = new Thread(threadTest);
        thread.setName("窗口三");
        thread.start();
        thread2.start();
        thread3.start();
    }
}
