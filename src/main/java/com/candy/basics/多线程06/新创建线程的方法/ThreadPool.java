package com.candy.basics.多线程06.新创建线程的方法;

import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author Candy
 * @create 2020-06-23 16:09
 * 创建线程方法四：使用线程池
 *
 */
public class ThreadPool {
    public static void main(String[] args) {
        /**
         * Executors：工具类、线程池的工厂类，用于创建并返回不同类型的线程池
         *  Executors.newCachedThreadPool()：创建一个可根据需要创建新线程的线程池
         *  Executors.newFixedThreadPool(n); 创建一个可重用固定线程数的线程池
         *  Executors.newSingleThreadExecutor() ：创建一个只有一个线程的线程池
         *  Executors.newScheduledThreadPool(n)：创建一个线程池，它可安排在给定延迟后运 行命令或者定期地执行
         */
        ThreadPoolExecutor executorService = (ThreadPoolExecutor)Executors.newFixedThreadPool(10);//创建10个线程
        //线程池配置属性
        executorService.setCorePoolSize(100);
        executorService.execute(new NumThread3());//适合用于实现Runnable接口的
        executorService.shutdown();
    }

}
class NumThread2 implements Callable<Integer> {
    @Override
    public Integer call() throws Exception {
        for (int i = 1; i <= 100; i++) {
            if(i % 2 == 0){
                System.out.println(Thread.currentThread().getName()+"："+i);
            }
        }
        return null;
    }
}


class NumThread3 implements Runnable {
    @Override
    public void run() {
        for (int i = 1; i <= 10000; i++) {
            if(i % 2 != 0){
                System.out.println(Thread.currentThread().getName()+"："+i);
            }
        }
    }
}