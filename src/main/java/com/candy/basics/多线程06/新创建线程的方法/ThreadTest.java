package com.candy.basics.多线程06.新创建线程的方法;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

/**
 * @author Candy
 * @create 2020-06-23 15:50
 * 创建线程的方法(旧)：继承Thread类，实现接口Runnable(都是对run()方法的重写)
 * 创建线程的方法(新)：实现接口Callable,重写call()方法
 *  相比run()方法，可以有返回值
 *  方法可以抛出异常
 *  支持泛型的返回值
 *  需要借助FutureTask类，比如获取返回结果
 */
public class ThreadTest{
    public static void main(String[] args) {
        NumThread numThread = new NumThread();

        FutureTask<Integer> futureTask = new FutureTask(numThread);

        Thread thread = new Thread(futureTask);
        thread.start();
        try {
            Integer sum = futureTask.get();
            System.out.println(sum);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

class NumThread implements Callable<Integer>{
    @Override
    public Integer call() throws Exception {
        int sum = 0;
        for (int i = 1; i <= 100; i++) {
            if(i % 2 == 0){
                sum += i;
//                System.out.println(sum);
            }
        }
        return sum;
    }
}
