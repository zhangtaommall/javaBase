package com.candy.basics.多线程06.线程死锁问题;

/**
 * @author Candy
 * @create 2020-06-22 17:28
 */
public class Exercise {

    private int money;

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public synchronized void cunMoney(int qian){
        if(qian > 0){
            money += qian;
            try {
                Thread.currentThread().sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+"余额是："+money);
        }
    }

    public static void main(String[] args) {
        Exercise exercise = new Exercise();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Thread.currentThread().setName("zt");
                for(int i=0;i<3;i++){
                    exercise.cunMoney(1000);
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                Thread.currentThread().setName("sj");
                for(int i=0;i<3;i++){
                    exercise.cunMoney(1000);
                }
            }
        }).start();

    }
}
