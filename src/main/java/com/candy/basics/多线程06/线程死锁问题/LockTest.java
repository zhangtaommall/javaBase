package com.candy.basics.多线程06.线程死锁问题;

import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Candy
 * @create 2020-06-22 15:06
 * 解决线程安全问题方法三：使用Lock锁
 */
public class LockTest {
    public static void main(String[] args) {
        Candy candy = new Candy();

        Thread thread1 = new Thread(candy);
        Thread thread2 = new Thread(candy);
        Thread thread3 = new Thread(candy);

        thread1.setName("窗口一");
        thread2.setName("窗口二");
        thread3.setName("窗口三");
        thread1.start();
        thread2.start();
        thread3.start();
    }

}
class Candy implements Runnable{

    private int piao = 100;
    private ReentrantLock lock = new ReentrantLock();

    @Override
    public void run() {
        while (true){
            try {
                //得到锁
                lock.lock();
                if(piao > 0){
                    try {
                        Thread.currentThread().sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("线程名称："+Thread.currentThread().getName()+"余票："+piao);
                    piao--;
                }else{
                    break;
                }
            } finally {
                //释放锁
                lock.unlock();
            }
        }
    }
}
