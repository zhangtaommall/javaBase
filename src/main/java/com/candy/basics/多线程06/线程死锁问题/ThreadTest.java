package com.candy.basics.多线程06.线程死锁问题;

/**
 * @author Candy
 * @create 2020-06-22 14:41
 */
public class ThreadTest {

    public static void main(String[] args) {
        StringBuffer buffer = new StringBuffer();
        StringBuffer buffer2 = new StringBuffer();
        /**
         * 1.2个线程同步执行
         * 2.线程1拿到锁buffer后阻塞0.1秒，线程2拿到锁2之后阻塞0.1秒；
         * 线程1等待锁2，线程2等待锁1，造成了死锁的结果
         */
        new Thread(){
            @Override
            public void run() {
                synchronized (buffer){
                    buffer.append("a");
                    buffer2.append("1");
                    try {
                        Thread.currentThread().sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    synchronized (buffer2){
                        buffer.append("b");
                        buffer2.append("2");
                        System.out.println(buffer);
                        System.out.println(buffer2);
                    }
                }
            }
        }.start();


        new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (buffer2){
                    buffer.append("c");
                    buffer2.append("3");
                    try {
                        Thread.currentThread().sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    synchronized (buffer){
                        buffer.append("d");
                        buffer2.append("4");
                        System.out.println(buffer);
                        System.out.println(buffer2);
                    }
                }
            }
        }).start();

    }

}
