package com.candy.basics.多线程06.线程通信;

/**
 * @author Candy
 * @create 2020-06-23 10:42
 * 2个线程交替打印1-100的数据
 * notify()：唤醒被wait()的线程，如果有多个，则唤醒优先级高的
 * wait()：当前线程进入阻塞状态，并且释放同步监视器(锁)
 * 方法wait()和notify()必须在同步代码块或者同步方法中使用；并且调用者也必须是同步监视器来调用
 */
public class ThreadTest {

    public static void main(String[] args) {
        Number number = new Number();

        Thread thread1 = new Thread(number);
        Thread thread2 = new Thread(number);
        thread1.setName("线程1");
        thread2.setName("线程2");
        thread1.start();
        thread2.start();
    }

}

class Number implements Runnable{

    private int number = 1;
    Object object = new Object();

    @Override
    public void run() {
        while (true){
            synchronized (this) {
                notify();//唤醒被wait()的线程，如果有多个，则唤醒优先级高的
                if(number <= 100){
                    System.out.println(Thread.currentThread().getName()+"："+number);
                    number++;
                    try {
                        wait();//当前线程进入阻塞状态，并且释放同步监视器(锁)
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }else{
                    break;
                }
            }
        }
    }
}
