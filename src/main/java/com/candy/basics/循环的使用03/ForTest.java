package com.candy.basics.循环的使用03;

/**
 * @author Candy
 * @create 2019-11-20 22:50
 */
public class ForTest {

    public static void main(String[] args) {
        /**
         * 输出：*
         *      **
         *      ***
         *      ****
         *      *****
         */
        for(int i = 1;i<=5;i++){//控制行数
            for(int j=0;j<i;j++){//控制列数
                System.out.print("*");
            }
            System.out.println();
        }
        System.out.println("==================");
        /**
         *     *
         *    * *
         *   * * *
         *  * * * *
         * * * * * *
         *  * * * *
         *   * * *
         *    * *
         *     *
         */
        //上半部分
        for(int i = 1;i<=5;i++){//控制行数
            for(int j=1;j<=5-i;j++){//控制列数
                System.out.print(" ");
            }
            for(int j=1;j<=i;j++){
                if(j == 5){
                    System.out.print("*");
                }else{
                    System.out.print("* ");
                }
            }
            for(int j=1;j<=4-i;j++){
                System.out.print(" ");
            }
            System.out.println("");
        }
        for(int i = 1;i<=4;i++){//控制行数
            for(int j=1;j<=i;j++){//控制列数
                System.out.print(" ");
            }
            for(int j=1;j<=5-i;j++){
                if(j == 5){
                    System.out.print("*");
                }else{
                    System.out.print("* ");
                }
            }
            for(int j=1;j<=i-1;j++){
                System.out.print(" ");
            }
            System.out.println();
        }


    }
}
