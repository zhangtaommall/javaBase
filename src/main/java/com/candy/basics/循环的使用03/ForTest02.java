package com.candy.basics.循环的使用03;

/**
 * @author Candy
 * @create 2019-11-21 20:46
 */
public class ForTest02 {
    public static void main(String[] args) {
        /**
         * 输出100以内的质数(只能被1和自己本身整除的数)
         */
        long timeMillis = System.currentTimeMillis();
        int count = 0;
        for(int i=2;i<=100000;i++){
            boolean flag = true;
            //Math.sqrt(i)或者i/2或者i
            for(int j=2;j<=Math.sqrt(i);j++){//优化一：开根号Math.sqrt(i)
                if(i%j == 0){
                    flag = false;
                    break;//优化二：跳出循环，避免没必要的计算
                }
            }
            if(flag){
                count++;
            }
        }
        System.out.println(count);
        System.out.println(System.currentTimeMillis()-timeMillis);

        /**
         * 一个数如果恰好等于它的因子之和，这个数就称为"完数"。（因子：除去这个数本身的约数）
         * 例如6=1＋2＋3.编程 找出1000以内的所有完数
         */
        for(int i=1;i<=100000;i++){
            int mun = 0;
            for(int j=1;j<=i/2;j++){
                if(i%j == 0){
                    mun += j;
                }
            }
            if(mun == i){
                System.out.println(i);
            }
        }
    }
}
