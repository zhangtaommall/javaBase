package com.candy.basics.数组的使用04;

/**
 * @author Candy
 * @create 2019-12-06 17:14
 */
public class ArrayDemo_04 {

    public static void main(String[] args) {
        String[] str = new String[]{"AA","ZZ","CC","NN","EE","MM","GG",};
        //数组的复制
        String[] strCope = new String[str.length];
        for (int i = 0; i < strCope.length; i++) {
            strCope[i] = str[i];
        }
        //数组反转(需要除以2，不然回换回去)
        for (int i = 0; i < str.length / 2; i++) {
            String temp = str[i];
            str[i] = str[str.length-i-1];
            str[str.length-i-1] = temp;
        }
        for (String s : str) {
            System.out.print(s+"\t");
        }
        //字符串的反转
        String stt = "zhangtao";
        String[] split = stt.split("");
        for (int i = 0; i < split.length / 2; i++) {
            String temp = split[i];
            split[i] = split[split.length-i-1];
            split[split.length-i-1] = temp;
        }
        stt = "";
        for (String s : split) {
            stt += s;
        }
        System.out.println(stt);
        //线性查找法(最普通的查找方法，挨个循环)

        //二分查找法(数据必须是有序的)
        int[] erfeng = new int[]{-22,-3,-1,5,44,66,78,123,444,567,888,3123};
        int shu = 888;
        int head = 0;//定义开头
        int end = erfeng.length-1;
        while (head <= end){
            //找中间的坐标
            int zhongjian = (head+end)/2;
            if(shu == erfeng[zhongjian]){
                System.out.println("索引位置为:"+zhongjian);
                break;
            }else if(erfeng[zhongjian] < shu){
                head = zhongjian + 1;
            }else{
                end = zhongjian -1;
            }
        }



    }

}
