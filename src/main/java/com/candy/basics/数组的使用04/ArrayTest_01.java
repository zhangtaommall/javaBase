package com.candy.basics.数组的使用04;

/**
 * @author Candy
 * @create 2019-11-22 10:15
 * 一维数组的使用
 * 1.数组的声明
 * 2.数组的赋值
 * 3.调用数组的元素
 * 4.如何获取数组的长度
 * 5.遍历数组
 * 6.数组元素的默认初始化值
 */
public class ArrayTest_01 {

    public static void main(String[] args) {
        //1.数组的声明
        int[] ids;
        //2-1.数组的赋值--静态初始化
        ids = new int[]{10,20,30,40};
        //2-2.数组的赋值--动态初始化(实际未赋值,只是声明了数组的长度，在内存中开辟的空间大小)
        String[] strs = new String[3];
        //3.通过下标(索引)的方式调用数组的元素,从0开始,如果是动态数组必须先赋值
        System.out.println(strs[0]);//输出位null
        strs[0]="孙健";
        strs[1]="张凯";
        strs[2]="施京利";
        //4.如何获取数组的长度
        System.out.println(strs.length);//3
        //5.遍历数组
        for (int i = 0; i < strs.length; i++) {
            System.out.println(strs[i]);
        }
        //6.数组元素的默认初始化值(只有基本数据类型有默认初始化值)
        //byte,short,int,long--->默认初始化值：0
        //float,double--->默认初始化值：0.0
        //char--->默认初始化值：0或'\u0000'  而非'0'
        //boolean--->默认初始化值：false
        char[] ints = new char[3];
        for (int i = 0; i < ints.length; i++) {
            System.out.println(ints[i]==0);//true
            System.out.println(ints[i]=='\u0000');//true
        }



    }

}
