package com.candy.basics.数组的使用04;

/**
 * @author Candy
 * @create 2019-11-22 17:53
 * 对于二维数组的理解，我们可以看成是一维数组 array1又作为另一个一维数组array2的元素而存在。
 * 其实，从数组底层的运行机制来看，其实没 有多维数组
 * 1.数组的声明
 * 2.数组的赋值
 * 3.调用数组的元素
 * 4.如何获取数组的长度
 * 5.遍历数组
 * 6.数组元素的默认初始化值
 */
public class ArrayTest_02 {
    public static void main(String[] args) {
        //1.二维数组的声明
        int[][] array;
        //2-1.二维数组的赋值--静态初始化
        array = new int[][]{{1,2,3},{4,6},{7,8,9}};
        //2-2.二维数组的赋值--动态初始化
        String[][] strss = new String[3][2];
        String[][] strss2 = new String[3][];
        //3.调用数组的元素
        System.out.println(array[1][1]);
//        System.out.println(strss2[1][1]);//报错 空指针
        //如果不想报错则需要：new
        strss2[1] = new String[3];
        System.out.println(strss2[1][1]);//不报错
        //4.如何获取数组的长度
        System.out.println(array.length);
        System.out.println(array[1].length);
        //5.遍历数组
        for(int i=0;i<array.length;i++){
            for(int j=0;j<array[i].length;j++){
                System.out.print(array[i][j]);
            }
            System.out.println();
        }
        //6.数组元素的默认初始化值
        int[][] defaultInt = new int[4][3];
        System.out.println(defaultInt[2][1]);//0
        System.out.println(defaultInt[2]);//地址值
        int[][] defaultInt2 = new int[4][];
        System.out.println(defaultInt2[2]);//不是地址值而是null  因为没有被初始化过

    }
}
