package com.candy.basics.数组的使用04;

/**
 * @author Candy
 * @create 2019-11-25 17:40
 * 杨辉三角
 * 1. 第一行有 1 个元素, 第 n 行有 n 个元素
 * 2. 每一行的第一个元素和最后一个元素都是 1
 * 3. 从第三行开始, 对于非第一个元素和最后一个元 素的元素。
 * 即： yanghui[i][j] = yanghui[i-1][j-1] + yanghui[i-1][j];
 */
public class ArrayTow_03 {

    public static void main(String[] args) {
//        int[][] array = new int[][]{{3,5,8},{12,9},{7,0,6,4}};
//        int num = 0;
//        for(int i=0;i<array.length;i++){
//            for(int j=0;j<array[i].length;j++){
//                num += array[i][j];
//            }
//        }
//        System.out.println(num);

//        int[][] yanghui = new int[10][];//动态初始化
//        //循环指定元素值(每行元素不一致，递增+1)
//        for (int i=0;i<yanghui.length;i++){
//            yanghui[i] = new int[i+1];
//            //因为首末元素都为1
//            yanghui[i][0] = 1;
//            yanghui[i][i] = 1;
//            //从非首末元素开始赋值
//            if(i>1){
//                for(int j=1;j<yanghui[i].length - 1;j++){//不能取最后一个元素需要-1
//                    yanghui[i][j] = yanghui[i-1][j-1] + yanghui[i-1][j];
//                }
//            }
//        }
//        for(int i=0;i<yanghui.length;i++){
//            for(int j=0;j<yanghui[i].length;j++){
//                System.out.print(yanghui[i][j]+" ");
//            }
//            System.out.println();
//        }

        //创建一个长度为6的int型数组，要求数组元素的值都在1-30之间，且是随机赋值。
        // 同时，要求 元素的值各不相同
        //随机数a-b之间包含a和b  公式：int random = (int)(Math.random()*(b-a+1)+a);
        int[] suiji = new int[20];
        suiji[0] = (int)(Math.random()*30+1);
        boolean flag = true;
        for(int i=1;i<suiji.length;i++){
            while(true){
                int random = (int)(Math.random()*30+1);
                for(int j=0;j<i;j++){
                    if(suiji[j] == random){
                        flag = false;
                        break;
                    }
                }
                if(flag){
                    suiji[i] = random;
                    break;
                }
                flag = true;
            }
        }
        //

    }

}
