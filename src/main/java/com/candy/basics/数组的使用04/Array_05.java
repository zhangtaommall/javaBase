package com.candy.basics.数组的使用04;

import java.util.Arrays;

/**
 * @author Candy
 * 衡量排序算法的优劣： 1.时间复杂度：分析关键字的比较次数和记录的移动次数
 * 2.空间复杂度：分析排序算法中需要多少辅助内存
 * 3.稳定性：若两个记录A和B的关键字值相等，但排序后A、B的先后次序保 持不变，则称这种排序算法是稳定的。
 * @create 2019-12-09 17:01
 */
public class Array_05 {
    public static void main(String[] args) {
        //冒泡排序(每个相邻的数进行比较)
        int[] maoPao = new int[]{-22,-43,666,-44,-4,444,432,564,634,23,5346,54,76,94,0};
        int[] maoPao2 = new int[]{-22,-43,666,-44,-4,444,432,564,634,23,5346,54,76,94,1};
            ////如果数组长度为n,比较次数为n-1,因为下标为0开始，所以需要maoPao.length - 1
            for(int i=0;i<maoPao.length - 1;i++){
                for(int j=0;j<maoPao.length -1 -i;j++){
                    //呼唤相邻的两个元素
                    if(maoPao[j] > maoPao[j+1]){
                        int temp = maoPao[j];
                        maoPao[j] = maoPao[j+1];
                        maoPao[j+1] = temp;
                    }
                }
            }
        System.out.println(Arrays.toString(maoPao));
        //工具类Arrays的使用
        //1.判断两个数组是否相等
        System.out.println(Arrays.equals(maoPao,maoPao2));
        //2.二分查找(数据必须有序)
        System.out.println(Arrays.binarySearch(maoPao,54));


    }
}
