package com.candy.basics.数组的使用04;

import java.util.Arrays;

/**
 * @author Candy
 * @create 2020-03-11 16:30
 */
public class Test {

    public static void main(String[] args) {

        int[] arr = new int[]{34,5,-12,0,77,3,-55};

        for(int i=0;i<arr.length-1;i++){
            for(int j=0;j<arr.length-1-i;j++){
                if(arr[j] > arr[j+1]){
                    int temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(arr));
    }

}
