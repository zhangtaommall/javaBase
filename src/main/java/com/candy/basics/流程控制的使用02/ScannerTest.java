package com.candy.basics.流程控制的使用02;

/**
 * @author Candy
 * @create 2019-11-15 19:10
 * 关于键盘输入Scanner的使用
 */
public class ScannerTest {

    public static void main(String[] args) {
        //如何从键盘获取不同类型的变量
//        Scanner scanner = new Scanner(System.in);
//        //通过对象的不同方法来获取指定类型的变量
//        int num = scanner.nextInt();
//        System.out.println(num);
//        String next = scanner.next();
//        System.out.println(next);

        //练习题
        /**
         * 假设你想开发一个玩彩票的游戏，程序随机地产生一个两位数的彩票，
         * 提示用户输入 一个两位数，然后按照下面的规则判定用户是否能赢。
         * 1)如果用户输入的数匹配彩票的实际顺序，奖金10 000美元。
         * 2)如果用户输入的所有数字匹配彩票的所有数字，但顺序不一致，奖金 3 000美元。
         * 3)如果用户输入的一个数字仅满足顺序情况下匹配彩票的一个数字，奖金1 000美元。
         * 4)如果用户输入的一个数字仅满足非顺序情况下匹配彩票的一个数字，奖金500美元。
         * 5)如果用户输入的数字没有匹配任何一个数字，则彩票作废
         */
        //随机数10-99之间包含10和99
        //随机数a-b之间包含a和b  公式：int random = (int)(Math.random()*(b-a+1)+a);
//        int random = (int)(Math.random()*90+10);
//        String str = String.valueOf(random);
//        System.out.println(str);
//        String[] split = str.split("");
//        String temp = split[0];
//        split[0] = split[1];
//        split[1] = temp;
//        String fande = split[0] + split[1];
//        Scanner scanner = new Scanner(System.in);
//        String num = scanner.next();
//        String[] num1 = num.split("");
//        if(str.equals(num)){
//            System.out.println("奖励10000美元");
//        }else if(fande.equals(num)){
//            System.out.println("奖励3000美元");
//        }else if(split[1].equals(num1[0])){
//            System.out.println("奖励1000美元");
//        }else if(num.contains(split[0]) || num.contains(split[1])){
//            System.out.println("奖励500美元");
//        }else{
//            System.out.println("彩票作废");
//        }
        //有n个数字，组成n位数，组成的n位数有可能相同有可能不同。问：他一共有多少种组合？

        int year = 2019;
//        从键盘分别输入年、月、日，判断这一天是当年的第几天
//        注：判断一年是否是闰年的标准：
        // 1）可以被4整除，但不可被100整除 或
        // 2）可以被400整除
        if((year % 4 == 0 && year % 100 != 0) || year % 400 ==0){
            System.out.println("闰年");
        }

        int m = 30;
        int n = 18;
        //最大公约数  取最小的数字(因为约数不能比他大)
        int min = m > n ? n : m;
        for(int i=min;i>=1;i--){
            if(m % i == 0 && n % i ==0){
                System.out.println("最大公约数："+i);
                break;
            }
        }
        //最小公倍数 取最大的数字
        int max = m > n ? m:n;
        for(int i=max;i<=m*n;i++){
            if(i % m == 0 && i % n ==0){
                System.out.println("最小公倍数："+i);
                break;
            }
        }
        //3位数水仙花 如：153=1*1*1 + 3*3*3 + 5*5*5;
        for(int i=100;i<1000;i++){
            int gewei = i%10;
            int shiWei = i%100/10;
            int baiWei = i%1000/100;
            if((gewei*gewei*gewei + shiWei*shiWei*shiWei + baiWei*baiWei*baiWei) == i){
                System.out.println(i);
            }
        }



    }
}
