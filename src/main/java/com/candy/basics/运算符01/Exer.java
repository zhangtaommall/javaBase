package com.candy.basics.运算符01;

/**
 * @author Candy
 * @create 2019-11-13 17:22
 * 随意给出一个整数，打印显示它的个位数，十位数，百位数的值。
 */
public class Exer {

    public static void main(String[] args) {
        int num = 2345342;
        int gewei = num%10;
        int shiWei = num%100/10;
        int baiWei = num%1000/100;
        int qianWei = num%10000/1000;
        int wanWei = num%100000/10000;
        int shiWanWei = num%1000000/100000;
        int baiWanWei = num/1000000;
        System.out.println("个位："+gewei);
        System.out.println("十位："+shiWei);
        System.out.println("百位："+baiWei);
        System.out.println("千位："+qianWei);
        System.out.println("万位："+wanWei);
        System.out.println("十万位："+shiWanWei);
        System.out.println("百万位："+baiWanWei);

        byte b = 127;
        byte b2 = ++b;
        System.out.println(b2);
    }

}
