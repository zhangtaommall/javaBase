package com.candy.basics.运算符01;

/**
 * @author Candy
 * @create 2019-11-13 16:37
 * 结论1：使用前++或--,后++或--,两者均不改变数据类型
 * 结论2：使用+=,-=,/=,*=,%=;均不会改变数据类型
 */
public class YunSuanFuTest {

    public static void main(String[] args) {
        //算术运算符
        int num1 = 12;
        int num2 = 5;
        double result = (double)num1/num2;//2.4
        double result2 = num1/(num2+0.0);//2.4
        double result3 = num1/num2;//2.0(丢失精度)
        System.out.println(result);
        System.out.println(result2);
        System.out.println(result3);
        //%：取余运算符
        //运算结果与被模数符号一致
        int res = num1%-num2;
        int res2 = -num1%-num2;
        System.out.println(res);//2
        System.out.println(res2);//-2
        //**************************************************
        //前++或--：先自增后运算
        //后++或--：先运算后自增
        //两者均不改变数据类型(不会变成int)
        byte a1 = 10;
        byte b1 = ++a1;
        System.out.println(a1+"==="+b1);//11===11
        int a2 = 10;
        int b2 = a2++;
        System.out.println(a2+"==="+b2);//11===10
        int i = 10;
        i += (i++) + (++i);//i = i+(i++)+(++i)
        System.out.println(i);

        //面试题
        boolean x = true;
        boolean y = false;
        short z = 42;
        if((z++==42) && (y=true)){
            z++;
        }
        if((x=false) || (++z==45)){
            z++;
        }
        System.out.println("z="+z);


    }


}
