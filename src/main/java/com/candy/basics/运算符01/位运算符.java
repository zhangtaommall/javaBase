package com.candy.basics.运算符01;

/**
 * @author Candy
 * @create 2019-11-15 14:28
 */
public class 位运算符 {

    public static void main(String[] args) {
        /**
         * 左移运算符号：<<     例如24<<4
         * 左移运算是2的N次幂(移动几位就是几次幂)，如移动4位就是乘以2的4次幂
         * 不可移动超过原始数据的二进制：0000 0100  最多左移动4位(byte数据类型)
         * 空位补0，被移除的高位丢弃，空缺位补0
         *
         * 右移运算符号：>>     例如24>>4
         * 右移运算是2的N次幂(移动几位就是几次幂)，如移动4位就是除以2的4次幂
         * 不可移动超过原始数据的二进制：0000 0100  最多右移动2位(byte数据类型)
         * 被移位的二进制最高位是0，右移后，空缺位补0； 最高位是1，空缺位补1
         *
         * 无符号右移动:>>>
         * 被移位二进制最高位无论是0或者是1，空缺位都用0补
         *
         *
         * 与运算:&
         * 二进制位进行&运算，只有1&1时结果是1，否则是0;
         *
         * 或运算:|
         * 二进制位进行 | 运算，只有0 | 0时结果是0，否则是1;
         *
         * 异或运算:^
         * 相同二进制位进行 ^ 运算，结果是0；1^1=0 , 0^0=0
         * 不相同二进制位 ^ 运算结果是1。1^0=1 , 0^1=1
         *
         * 取反运算:~
         * 正数取反，各二进制码按补码各位取反
         * 负数取反，各二进制码按补码各位取反
         */
        int i = 24<<4;//24*2的4次方
        System.out.println("24<<4："+i);//384
        int i2 = -24<<4;//-24*2的4次方
        System.out.println("-24<<4："+i2);//-384

        int i3 = 2244>>3;//2244/2的3次方
        System.out.println("2244>>3："+i3);//280
        int i4 = -2244>>3;//-2244/2的3次方
        System.out.println("-2244>>3："+i4);//-281

        int n1 = 5;
        int n2 = 22;
        int n3 = 333;
        int x = n1 > n2 ? (n1 > n3 ? n1:n3) : (n2 > n3 ? n2 : n3);
        System.out.println(x);

    }

}
