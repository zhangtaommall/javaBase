package com.candy.basics.面向对象05;

/**
 * @author Candy
 * @create 2020-04-13 10:19
 */
public class Object01 {

    public static int count= 0;

    public static void main(String[] args) {
        Object01 object01 = new Object01();
        String str = "dsdw";
        object01.addStr(str);
        System.out.println(str);

        int[] arr = new int[]{2,1};
        char[] arr2 = new char[]{'a','c','b'};
//        System.exit(0);
        System.out.println(arr);
        System.out.println(arr2);
        //递归的用法
        /**
         * 计算1 - n 之间所有自然数的乘积
         */
        int i = object01.sum1(10);
        System.out.println(i);
        /**
         * 已知一个数列：f(0)=1,f(1)=4,f(n+2)=2*f(n+1) + f(n);
         * 其中n是大于0的整数，求f(10)的值
         */
        //f(2)=2f(1)+f(0)
        //f(3)=2f(2)+f(1)
        //f(4)=2f(3)+f(2)
        //f(5)=2f(4)+f(3)
        int f = object01.f(10);
        System.out.println(f);
        System.out.println(count);

    }
    public void addStr(String str){
        String s = str + "ddefe";
    }

    public int f(int n){//····
        count++;
        if(n == 0){
            return 1;
        }else if(n == 1){
            return 4;
        }else{
            return 2 * f(n -1) + f(n - 2);
        }
    }

    public int sum1(int n){
        if(n == 1){
            return 1;
        }else{
            return sum1(n -1) * n;
        }

    }
}
