package com.candy.basics.面向对象05.继承;

/**
 * @author Candy
 * @create 2020-05-11 16:45
 */
public class Persion extends PersionObj{

    String name = "1";

    public Persion(){
        System.out.println("========Persion========");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    private void eat(){
        System.out.println("=======Persion=========");
    }
}
