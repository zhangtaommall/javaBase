package com.candy.basics.面向对象05.继承;

/**
 * @author Candy
 * @create 2020-05-11 16:46
 */
public class Student extends Persion {

    String name ="sa";

    public Student(){
        System.out.println("========Student========");
    }

    public static void main(String[] args) {
//        Student student = new Student();
//        student.eat();
        Object persion1 = new Student();
        Persion persion = (Persion)persion1;
//        if(new Student() instanceof Persion){
//            Student student = (Student)persion;
//        }
//        if(persion instanceof Persion){
//            System.out.println("是Persion了");
//        }
        String s1 = "asd";
        String s2 = "asd";
        String s3 = new String("asd");
        String s4= new String("asd");
        System.out.println(s1 == s2);//true
        System.out.println(s1 == s3);//false
        System.out.println(s4 == s3);//false
        System.out.println(s1.equals(s2));//true
        System.out.println(s1.equals(s3));//true
        System.out.println(s4.equals(s3));//true
    }

    private void eat(){
        System.out.println("==========-Student--======"+super.id);
    }


}
