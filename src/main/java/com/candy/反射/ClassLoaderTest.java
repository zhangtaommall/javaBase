package com.candy.反射;

import org.junit.Test;

import java.io.InputStream;
import java.util.Properties;

/**
 * @author Candy
 * @create 2020-09-18 14:25
 * 类的加载器：
 * 1.引导类加载器：负责加载JAVA的核心类库，不可直接获取
 * 2.拓展类加载器：负责加载jre的lib包下的一些jar包，可以通过系统类加载器获取
 * 3.系统类加载器：负责加载自定义类的，可以直接获取
 */
public class ClassLoaderTest {

    @Test
    public void test(){
        //对于自定义类，可以使用系统类加载器
        ClassLoader classLoader = ClassLoaderTest.class.getClassLoader();
        //通过系统类加载器获取拓展类加载器
        ClassLoader parent = classLoader.getParent();
        //无法获取引导类加载器，值为null
        ClassLoader classLoader1 = String.class.getClassLoader();
    }

    /**
     * Properties读取配置文件
     */
    @Test
    public void test2()throws Exception{
        Properties properties = new Properties();
        //方式一：
//        properties.load(new FileInputStream("ds.properties"));

        //方式二：通过系统类加载器读取(默认相对路径需要在src下,暂时行不通)
        ClassLoader classLoader = ClassLoaderTest.class.getClassLoader();
        InputStream resourceAsStream = classLoader.getResourceAsStream("ds1.properties");
        properties.load(resourceAsStream);
        String property = properties.getProperty("DSDB.user");
        System.out.println(property);





    }

}
