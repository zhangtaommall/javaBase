package com.candy.反射;

/**
 * @author Candy
 * @create 2020-09-08 16:54
 */
public class Person {

    public String ids;
    private String name;
    private Integer age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "ids='" + ids + '\'' +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public Person() {
    }

    private Person(String name) {
        this.name = name;
    }

    public Person(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    public static void show(){
        System.out.println("你好我是孙健");
    }
    public void show2(){
        System.out.println("你好我是张凯");
    }

    private String show3(String str){
        System.out.println("你好我是show3"+str);
        return str;
    }
}
