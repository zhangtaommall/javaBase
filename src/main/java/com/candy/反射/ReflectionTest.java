package com.candy.反射;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * @author Candy
 * @create 2020-09-08 16:54
 * 通过反射"获取"指定public的构造器：getConstructor(args ...)
 * 通过反射"获取"指定构造器(包括私有的)：getDeclaredConstructor(args ...)
 * 通过反射创建对象：newInstance(args ...)
 * 通过反射"获取"指定public属性：getField("属性名")
 * 通过反射"获取"指定属性(包括私有的)：getDeclaredField("属性名")
 * 通过反射"获取"指定public方法：getMethod("方法名",方法参数 ...);
 * 通过反射"获取"指定方法(包括私有的)：getDeclaredMethod("方法名",方法参数 ...)
 *
 * 只要涉及到调用私有的结构，需要加：setAccessible(true);
 *
 * 体现运行时的动态性：动态代理
 */
public class ReflectionTest {

    @Test
    public void test() throws Exception{
        //获取对象
        Class personClass = Person.class;
        //获取构造器(Person声明了3个构造器)
        Constructor constructor1 = personClass.getConstructor(String.class, Integer.class);
        //如果涉及到调用私有的构造器
        Constructor constructor2 = personClass.getDeclaredConstructor(String.class);
        //通过构造器造对象
        Person person = (Person)constructor1.newInstance("Tom", 12);
        System.out.println(person);
        //调用私有构造器
        constructor2.setAccessible(true);
        Person person1 = (Person)constructor2.newInstance("Jerry");
        System.out.println("调用私有的构造器："+person1);
        //调用对象指定的属性
        Field ids = personClass.getField("ids");
        ids.set(person,"ids");
        //调用对象指定的属性(私有的属性)
        Field age = personClass.getDeclaredField("age");
        age.setAccessible(true);
        age.set(person,1);
        Field name = personClass.getDeclaredField("name");
        name.setAccessible(true);
        name.set(person,"aas");
        System.out.println(person);
        //调用对象指定的方法
        Method show = personClass.getMethod("show");
        show.invoke(person);
        //调用对象指定的方法(私有方法)
        Method show2 = personClass.getDeclaredMethod("show2");
        show2.setAccessible(true);
        show2.invoke(person);
        Method show3 = personClass.getDeclaredMethod("show3",String.class);
        show3.setAccessible(true);
        //获取返回值
        String invoke = (String)show3.invoke(person, "中国");
        System.out.println(invoke);
    }

    /**
     * 获取Class实例的方法:只会存在一个运行时类,可以通过不同方式获取.
     * @throws Exception
     */
    @Test
    public void test2() throws Exception{
        //方式一：调用运行时类的属性.class
        Class<Person> personClass = Person.class;
        System.out.println(personClass);
        //方式二：通过运行时类的对象调用getClass()方法
        Person person = new Person();
        Class<? extends Person> aClass2 = person.getClass();
        System.out.println(aClass2);
        //方式三：调用class的静态方法forName(String classPath)全类名方式
        Class<?> aClass3 = Class.forName("com.candy.反射.Person");
        System.out.println(aClass3);
        System.out.println(personClass == aClass2);
        System.out.println(personClass == aClass3);
        //方式四：类的加载器ClassLoader
        ClassLoader classLoader = ReflectionTest.class.getClassLoader();
        Class<?> aClass4 = classLoader.loadClass("com.candy.反射.Person");
        System.out.println(aClass4);
        System.out.println(personClass == aClass4);
    }



}
