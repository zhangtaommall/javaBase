package com.candy.反射;

import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * @author Candy
 * @create 2020-09-17 17:00
 * "调用"运行时类指定的结构：属性、方法、构造器
 */
public class Test2 {


    /**
     * "调用"运行时类的属性
     */
    @Test
    public void test()throws Exception{
        Class<Person> personClass = Person.class;
        Person person = personClass.newInstance();

        Field name = personClass.getDeclaredField("name");
        name.setAccessible(true);
        name.set(person,"孙健");
        Object o = name.get(person);
        System.out.println(o);
    }

    /**
     * "调用"运行时类的方法
     * @throws Exception
     */
    @Test
    public void test2()throws Exception{
        Class<Person> personClass = Person.class;
        Person person = personClass.newInstance();

        Method show3 = personClass.getDeclaredMethod("show3", String.class);
        show3.setAccessible(true);
        //调用方法
        Object invoke = show3.invoke(person, "张凯");
        System.out.println(invoke);
        //调用静态方法
        Method show = personClass.getDeclaredMethod("show");
        show.invoke(personClass);

    }
}
