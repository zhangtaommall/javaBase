package com.candy.反射.exer;

import cn.hutool.core.collection.CollUtil;
import org.junit.Test;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Candy
 * @create 2020-09-10 16:15
 */
public class ReflectionTest2 {

    /**
     * 获取运行时类的属性结构
     * @throws Exception
     */
    @Test
    public void test() throws Exception{
//        Class<Work> workClass = Work.class;
//        /**
//         * 获取运行时类的所有属性getFields()：只能获取自身及父类中所有public修饰的属性
//         * getDeclaredFields()：只能获取自身中所有属性(任何权限且不包含父类)
//         */
//        Field[] fields = workClass.getFields();
//        for (Field field : fields) {
//            System.out.println(field);
//        }
//        System.out.println("------------");
//        Field[] declaredFields = workClass.getDeclaredFields();
//        for (Field declaredField : declaredFields) {
//            //获取属性的权限修饰符：public(1)  private(2)  默认权限(0) protected(4)
////            System.out.println(Modifier.toString(declaredField.getModifiers()));
//            //获取属性的类型
//            String name = declaredField.getType().getName();
////            System.out.println(name);
//            //获取属性的名称
////            System.out.println(declaredField.getName());
//            System.out.println(Modifier.toString(declaredField.getModifiers())+"\t"+name+"\t"+declaredField.getName());
//        }
    }

    /**
     * 获取运行时类的方法结构
     * @throws Exception
     */
    @Test
    public void test2() throws Exception{
        Class<Work> workClass = Work.class;
        /**
         *  获取运行时类的所有方法getMethods()：只能获取自身及父类中所有public修饰的方法
         *  获取运行时类的所有方法getDeclaredMethods()：只能获取自身所有方法(任何权限且不包含父类)
         */
        Method[] methods = workClass.getMethods();
        for (Method method : methods) {
            System.out.println(method);
        }
        System.out.println("------------------------");
        Method[] declaredMethods = workClass.getDeclaredMethods();
        for (Method declaredMethod : declaredMethods) {
            System.out.println(declaredMethod);
            //1.获取方法上声明的注解(生命周期要够，RetentionPolicy.RUNTIME)
            Annotation[] annotations = declaredMethod.getAnnotations();
            for (Annotation annotation : annotations) {
                System.out.println("方法上声明的注解："+annotation);
            }
            //2.获取方法的权限修饰符
            //获取属性的权限修饰符：public(1)  private(2)  默认权限(0) protected(4)
            int modifiers = declaredMethod.getModifiers();
            System.out.println(modifiers+"========>"+Modifier.toString(modifiers));

            //3.获取方法的返回值类型
            Class<?> returnType = declaredMethod.getReturnType();
            System.out.println("方法的返回值类型："+returnType.getName());

            //4.获取方法的方法名
            System.out.println("方法名称："+declaredMethod.getName());

            //5.获取方法的形参列表
            Class[] parameterTypes = declaredMethod.getParameterTypes();
            if(parameterTypes != null && parameterTypes.length != 0){
                for (Class parameterType : parameterTypes) {
                    System.out.println("方法形参列表："+parameterType.getName());
                }
            }
            //6.获取方法抛出的异常类型
            Class<?>[] exceptionTypes = declaredMethod.getExceptionTypes();
            for (Class<?> exceptionType : exceptionTypes) {
                System.out.println("方法抛出的异常："+exceptionType.getName());
            }
        }
    }

    /**
     * 获取运行时类的构造器
     * @throws Exception
     */
    @Test
    public void test3() throws Exception{
        Class<Work> workClass = Work.class;
        //1.获取运行时类声明为public的构造器
        Constructor<?>[] constructors = workClass.getConstructors();
        for (Constructor<?> constructor : constructors) {
            System.out.println(constructor);
        }
        //2.获取运行时类全部的构造器
//        Constructor<?>[] declaredConstructors = workClass.getDeclaredConstructors();
//        for (Constructor<?> declaredConstructor : declaredConstructors) {
//            System.out.println(declaredConstructor);
//        }

    }

    /**
     * 获取运行时类的父类,实现的接口
     * @throws Exception
     */
    @Test
    public void test4(){
        Class<Work> workClass = Work.class;
        //1.获取运行时类的父类
        Class<? super Work> superclass = workClass.getSuperclass();
        System.out.println(superclass);
        //2.获取运行时类带泛型的父类
        Type genericSuperclass = workClass.getGenericSuperclass();
        System.out.println(genericSuperclass);
        //3.获取运行时类父类的泛型
        ParameterizedType parameterizedType = (ParameterizedType)genericSuperclass;
        Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
        for (Type actualTypeArgument : actualTypeArguments) {
            System.out.println(actualTypeArgument.getTypeName());
        }
        //获取运行时类实现的接口
        Class<?>[] interfaces = workClass.getInterfaces();
        for (Class<?> anInterface : interfaces) {
            System.out.println("运行时类实现的接口："+anInterface);
        }
        Class<?>[] interfaces1 = workClass.getSuperclass().getInterfaces();
        for (Class<?> aClass : interfaces1) {
            System.out.println("运行时类父类实现的接口："+aClass);
        }
    }

    /**
     * 获取运行时类所在的包,和类上声明的注解
     * @throws Exception
     */
    @Test
    public void test5(){
        Class<Work> workClass = Work.class;
        Package aPackage = workClass.getPackage();
        System.out.println(aPackage);
        //获取运行时类声明的注解
        Annotation[] annotations = workClass.getAnnotations();
        for (Annotation annotation : annotations) {
            System.out.println(annotation);
        }

    }
}
