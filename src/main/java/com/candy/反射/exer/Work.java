package com.candy.反射.exer;

/**
 * @author Candy
 * @create 2020-09-09 17:46
 */
@MyAnnotation(value = "hi")
public class Work extends WorkPrant<String> implements Comparable<String>,Myinterface{

    private String name;
    int age;
    public Integer id;

    public Work() {
    }
    @MyAnnotation(value = "aaa")
    private Work(String name) {
        this.name = name;
    }

    Work(String name, int age) {
        this.name = name;
        this.age = age;
    }
    @MyAnnotation(value = "美国")
    private String show(String nation){
        System.out.println("我是"+nation+"人");
        return nation;
    }

    public String display(String display) throws Exception{
        return display;
    }

    @Override
    public void info() {
        System.out.println("我在卓优工作");
    }

    @Override
    public int compareTo(String o){
        return 0;
    }
}
