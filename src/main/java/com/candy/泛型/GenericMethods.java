package com.candy.泛型;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Candy
 * @create 2020-08-24 17:10
 *
 * 泛型方法：不需要一定是泛型类,两者无关联
 */
public class GenericMethods {

    //泛型方法可以是静态的
    public static <T> List<T> fanXing(List<T> tList){
        List<T> list = new ArrayList<>();
        for (T t1 : tList) {
            list.add(t1);
        }
        return list;
    }

}
