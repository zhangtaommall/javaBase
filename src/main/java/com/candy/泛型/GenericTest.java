package com.candy.泛型;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Candy
 * @create 2020-08-24 10:49
 */
public class GenericTest {

    @Test
    public void test(){
        //实例化时可以指定泛型
        Order<String> order = new Order<>("孙健",31,"卓优");

        //测试泛型方法
        GenericMethods methods = new GenericMethods();
        List<Integer> integerList = new ArrayList<>();
        integerList.add(11);
        integerList.add(22);
        integerList.add(33);
        List<Integer> integers = methods.fanXing(integerList);

        List<Object> list = null;
        ArrayList<String> list2 = null;
//        list = list2;//报错
    }

    /**
     * 通配符的使用：?
     */
    @Test
    public void test1(){
        List<Object> list1 = null;
        List<String> list2 = null;

        List<?> list = new ArrayList<>();//通配符的List不能添加add数据,但是可以加null
        list = list1;
        list = list2;
//        list.add("?");//报错
    }
    public void showFor(List<?> list){
        Iterator<?> iterator = list.iterator();
        while (iterator.hasNext()){
            Object next = iterator.next();
            System.out.println(next);
        }
    }

    /**
     * 有限制条件的通配符使用
     */
    @Test
    public void test2(){
        //只能赋值Order及其子类
        List<? extends Order> list1 = null;
        //只能赋值Order及其父类
        List<? super Order> list2 = null;

        List<Order> list3 = null;
        List<SubOrder> list4 = null;
        List<Object> list5 = null;

        list1 = list3;
        Order order = list1.get(0);
        list1 = list4;
//        list1 = list5;//不可以，因为Object大于Order类

        list2 = list3;
        Object o = list2.get(0);
//        list2 = list4;//不可以，因为SubOrder小于Order类
        list2 = list5;

    }


}
