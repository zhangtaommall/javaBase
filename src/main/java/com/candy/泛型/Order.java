package com.candy.泛型;

/**
 * 泛型类
 * @author Candy
 * @create 2020-08-24 10:40
 */
public class Order<T> {

    private String name;
    private Integer ids;

    //类的内部使用
    T t;

    public Order() {
    }

    public Order(String name, Integer ids, T t) {
        this.name = name;
        this.ids = ids;
        this.t = t;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIds() {
        return ids;
    }

    public void setIds(Integer ids) {
        this.ids = ids;
    }

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }
}
