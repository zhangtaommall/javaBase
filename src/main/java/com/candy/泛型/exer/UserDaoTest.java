package com.candy.泛型.exer;

import org.junit.Test;

import java.util.List;
import java.util.Map;

/**
 * @author Candy
 * @create 2020-08-26 11:21
 */
public class UserDaoTest {

    @Test
    public void test(){

        Dao<User> userDao = new Dao<>();
        userDao.save("1001",new User(1,21,"孙健"));
        userDao.save("1002",new User(2,31,"张凯"));


        User user = userDao.get("1002");
        System.out.println(user);

        userDao.update("1002",new User(2,31,"张凯22"));

        List<User> list = userDao.list();
        System.out.println(list.toString());

        userDao.delete("1002");

        List<User> list2 = userDao.list();
        System.out.println(list2.toString());

    }


}
