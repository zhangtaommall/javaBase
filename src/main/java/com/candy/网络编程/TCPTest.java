package com.candy.网络编程;

import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author Candy
 * @create 2020-09-07 15:45
 * TCP网络编程：发送文本
 */
public class TCPTest {

    //客户端
    @Test
    public void client(){
        Socket socket = null;
        OutputStream outputStream = null;
        try {
            //创建Socket对象，指明服务器端的ip和端口号
            InetAddress inetAddress = InetAddress.getByName("127.0.0.1");
            socket = new Socket(inetAddress,8888);

            //获取输出流，给服务器发送消息
            outputStream = socket.getOutputStream();
            outputStream.write("孙健GG你好".getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //资源关闭
            try {
                if(outputStream != null){
                    outputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if(socket != null){
                    socket.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    //服务端
    @Test
    public void server(){
        ServerSocket serverSocket = null;
        Socket accept = null;
        InputStreamReader inputStreamReader = null;
        try {
            //创建ServerSocket服务器端对象,指明端口号
            serverSocket = new ServerSocket(8888);
            //2.可以接收客户端消息
            accept = serverSocket.accept();
            //获取输入流，读取客户端的消息
            InputStream inputStream = accept.getInputStream();
            inputStreamReader = new InputStreamReader(inputStream);
            char[] bytes = new char[2];
            int len;
            while ((len = inputStreamReader.read(bytes)) != -1){
                String str = new String(bytes,0,len);
                System.out.print(str);
            }
//            System.out.println("收到了来自："+accept.getInetAddress().getHostAddress());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //关闭资源
            try {
                if(inputStreamReader != null){
                    inputStreamReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if(accept != null){
                    accept.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if(serverSocket != null){
                    serverSocket.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
