package com.candy.网络编程;

import org.junit.Test;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author Candy
 * @create 2020-09-07 17:28
 * TCP网络编程：
 */
public class TCPTest3 {


    //客户端
    @Test
    public void client() {
        Socket socket = null;
        OutputStream outputStream = null;
        FileInputStream fileInputStream = null;
        InputStreamReader inputStreamReader = null;
        try {
            //1.创建Socket对象，指明服务器端的ip和端口号
            InetAddress inetAddress = InetAddress.getByName("127.0.0.1");
            socket = new Socket(inetAddress, 8888);

            //2.获取输出流，把文件读入到内存中，然后写出文件到服务器
            outputStream = socket.getOutputStream();
            fileInputStream = new FileInputStream("吃烧烤的小姐姐.jpg");

            byte[] bytes = new byte[1024];
            int len;
            while ((len = fileInputStream.read(bytes)) != -1){
                outputStream.write(bytes,0,len);
            }
            //断开输出，表示传输完毕(因为read()方法是个阻塞方法，不说明读取完毕，会一直执行)
            socket.shutdownOutput();

            //接收服务端的反馈(读取数据并显示)
            InputStream inputStream = socket.getInputStream();
            inputStreamReader = new InputStreamReader(inputStream);
            char[] chars = new char[2];
            int len1;
            while ((len1 = inputStreamReader.read(chars)) != -1){
                String str = new String(chars,0,len1);
                System.out.print(str);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //3.关闭资源
            try {
                if(inputStreamReader != null){
                    inputStreamReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if(fileInputStream != null){
                    fileInputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if(outputStream != null){
                    outputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if(socket != null){
                    socket.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    //服务端
    @Test
    public void server() {
        ServerSocket serverSocket = null;
        Socket accept = null;
        InputStream inputStream = null;
        FileOutputStream fileOutputStream = null;
        OutputStream outputStream = null;
        try {
            //1.创建ServerSocket服务器端对象,指明端口号
            serverSocket = new ServerSocket(8888);
            //2.可以接收客户端消息
            accept = serverSocket.accept();

            //3.获取输入流，读取客户端的文件，并且写出到本地存储
            inputStream = accept.getInputStream();
            fileOutputStream = new FileOutputStream("吃烧烤的小姐姐2.jpg");
            byte[] bytes = new byte[1024];
            int len;
            while ((len = inputStream.read(bytes)) != -1){
                fileOutputStream.write(bytes,0,len);
            }
            System.out.println("图片传输完成");
            //给客户端输出文本，表示已经收到消息
            outputStream = accept.getOutputStream();
            outputStream.write("收到了图片，谢谢".getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //4.关闭资源
            try {
                if(outputStream != null){
                    outputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if(fileOutputStream != null){
                    fileOutputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if(inputStream != null){
                    inputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if(accept != null){
                    accept.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if(serverSocket != null){
                    serverSocket.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
