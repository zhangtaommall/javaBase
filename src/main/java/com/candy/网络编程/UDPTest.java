package com.candy.网络编程;

import org.junit.Test;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * @author Candy
 * @create 2020-09-08 10:24
 * UDP网络编程
 */
public class UDPTest {

    //发送端
    @Test
    public void sender() {
        DatagramSocket datagramSocket = null;
        try {
            datagramSocket = new DatagramSocket();

            String str = "我是UTP方式来小明家玩";
            InetAddress localHost = InetAddress.getLocalHost();
            DatagramPacket datagramPacket = new DatagramPacket(str.getBytes(),0,str.getBytes().length,localHost,8888);

            datagramSocket.send(datagramPacket);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(datagramSocket != null){
                    datagramSocket.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //接收端
    @Test
    public void receiver(){
        DatagramSocket datagramSocket = null;
        try {
            datagramSocket = new DatagramSocket(8888);

            byte[] bytes = new byte[100];
            DatagramPacket datagramPacket = new DatagramPacket(bytes,0,bytes.length);
            datagramSocket.receive(datagramPacket);
            System.out.println(new String(datagramPacket.getData(),0,datagramPacket.getLength()));
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            datagramSocket.close();
        }
    }


}
