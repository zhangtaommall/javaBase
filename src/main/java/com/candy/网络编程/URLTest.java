package com.candy.网络编程;

import org.junit.Test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * @author Candy
 * @create 2020-09-08 10:53
 */
public class URLTest {

    /**
     * public String getProtocol(  )     获取该URL的协议名
     * public String getHost(  )        获取该URL的主机名
     * public String getPort(  )        获取该URL的端口号
     * public String getPath(  )        获取该URL的文件路径
     * public String getFile(  )         获取该URL的文件名
     * public String getQuery(   )      获取该URL的查询名
     */
    @Test
    public void Test() throws Exception{
        URL url = new URL("http://desk.zol.com.cn/showpic/1920x1080_85332_273.html");

        System.out.println(url.getProtocol());
        System.out.println(url.getHost());
        System.out.println(url.getPort());
        System.out.println(url.getPath());
        System.out.println(url.getFile());
        System.out.println(url.getQuery());
    }

    @Test
    public void Test2(){
        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;
        FileOutputStream fileOutputStream = null;
        try {
            URL url = new URL("http://img14.360buyimg.com/n0/jfs/t1/145929/15/7840/108761/5f56c654Ed83b888b/d6699e3a869b17d8.jpg");
            urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.connect();
            inputStream = urlConnection.getInputStream();

            fileOutputStream = new FileOutputStream("baidu.jpg");

            byte[] bytes = new byte[1024];
            int len;
            while ((len = inputStream.read(bytes)) != -1){
                fileOutputStream.write(bytes,0,len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(fileOutputStream != null){
                    fileOutputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if(inputStream != null){
                    inputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            urlConnection.disconnect();
        }
    }

}
