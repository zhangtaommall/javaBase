package com.candy.设计模式.代理模式;

/**
 * @author Candy
 * @create 2020-06-10 10:18
 * 静态代理
 */
public class ProxyTest {

    public static void main(String[] args) {
        ProxyGame proxyGame = new ProxyGame(new MeGame());
        proxyGame.play();
    }

}

interface Game{

    void play();
}


class MeGame implements Game{

    @Override
    public void play() {
        System.out.println("我开始PUBG");
    }
}

class ProxyGame implements Game{

    private MeGame meGame;

    public ProxyGame(MeGame meGame) {
        this.meGame = meGame;
    }

    public void buyComputer(){
        System.out.println("购买带电脑");
    }

    @Override
    public void play() {
        buyComputer();
        meGame.play();
    }
}

