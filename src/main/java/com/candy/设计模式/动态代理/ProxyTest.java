package com.candy.设计模式.动态代理;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author Candy
 * @create 2020-09-18 17:03
 * 动态代理举例
 *
 */

//接口
interface Play{
    void duanWei();//升级段位
}

//被代理类
class PlayLoL implements Play{

    @Override
    public void duanWei() {
        System.out.println("升级LOL段位");
    }
}

/**
 * 生成动态代理类需要？
 * 1.如何根据加载到内存中的被代理类，动态创建代理对象？
 * 2.如何通过代理类对象，去调用被代理类里面对的方法？
 *
 * 用于生产代理类的对象
 */
class ProxyFactory{

    //通过此方法返回一个代理对象(完成代理对象的创建)
    public static Object getProxyInstance(Object obj){//传入被代理类的参数，用于生成代理类
        return Proxy.newProxyInstance(obj.getClass().getClassLoader()
                ,obj.getClass().getInterfaces(),new MyInvocationHandler(obj));
    }
}

class MyInvocationHandler implements InvocationHandler{

    private Object obj;//需要使用被代理类赋值

    public MyInvocationHandler(Object obj) {
        this.obj = obj;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        return method.invoke(obj, args);
    }
}

public class ProxyTest {
    public static void main(String[] args) {
        PlayLoL playLoL = new PlayLoL();

        //生成代理对象
        Play proxyInstance = (Play)ProxyFactory.getProxyInstance(playLoL);
        proxyInstance.duanWei();

        //还可以代理生产手机
        iPhone iPhone = new iPhone();
        ProductionMobile proxyInstance2 = (ProductionMobile)ProxyFactory.getProxyInstance(iPhone);
        proxyInstance2.Mobile();

    }
}
