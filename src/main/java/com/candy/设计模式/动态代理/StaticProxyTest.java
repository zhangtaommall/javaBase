package com.candy.设计模式.动态代理;

import org.junit.Test;

/**
 * @author Candy
 * @create 2020-09-18 16:41
 * 举例静态代理
 * 特点：代理类和被代理类在编译期间就确定下来
 */
public class StaticProxyTest {

    @Test
    public void test(){
        iPhone iPhone = new iPhone();

        ProxyClothFactory proxyClothFactory = new ProxyClothFactory(iPhone);
        proxyClothFactory.Mobile();
    }

}

//接口
interface ProductionMobile{
    void Mobile();
}

//代理类
class ProxyClothFactory implements ProductionMobile{

    private ProductionMobile productionMobile;

    public ProxyClothFactory(ProductionMobile productionMobile) {
        this.productionMobile = productionMobile;
    }

    @Override
    public void Mobile() {
        System.out.println("采购原件");
        productionMobile.Mobile();
        System.out.println("生产完毕");
    }
}

class iPhone implements ProductionMobile{

    @Override
    public void Mobile() {
        System.out.println("生产苹果手机");
    }
}
