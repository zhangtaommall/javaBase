package com.candy.设计模式.单例;

/**
 * @author Candy
 * @create 2020-05-13 16:49
 */
public class SingletonTest {

    private static SingletonTest singletonTest;

    private SingletonTest() {}

    public synchronized static SingletonTest getInitSingletonTest(){
        //懒汉模式：线程不安全的，需要改进
        if(singletonTest == null){
            singletonTest = new SingletonTest();
        }
        return singletonTest;
    }

    public static void main(String[] args) {
        SingletonTest initSingletonTest = SingletonTest.getInitSingletonTest();
        SingletonTest initSingletonTest2 = SingletonTest.getInitSingletonTest();
        System.out.println(initSingletonTest == initSingletonTest2);
    }
}
