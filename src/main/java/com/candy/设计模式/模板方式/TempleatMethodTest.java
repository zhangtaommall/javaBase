package com.candy.设计模式.模板方式;

/**
 * @author Candy
 * @create 2020-06-02 14:47
 */
public class TempleatMethodTest {

    public static void main(String[] args) {
        BankTempleatMethod method = new DrawMoney();
        method.process();

    }

}

abstract class BankTempleatMethod {

    public void takeNumber(){
        System.out.println("取号排队");
    }
    public abstract void transact();

    public void evaluate(){
        System.out.println("评分");
    }

    public final void process(){
        this.takeNumber();
        this.transact();
        this.evaluate();
    }

}

class DrawMoney extends BankTempleatMethod {

    @Override
    public void transact() {
        System.out.println("取款");
    }
}

class Manage extends BankTempleatMethod {

    @Override
    public void transact() {
        System.out.println("我要理财2000");
    }
}
