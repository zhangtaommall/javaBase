package com.candy.集合.collection.list;

import com.candy.baseutil.compore.Person;
import org.junit.Test;

import java.util.*;

/**
 * @author Candy
 * @create 2020-07-28 18:00
 */
public class CollectionTest {

    /**
     * ArrayList、LinkedList、Vector异同
     * 同：都实现与List接口，都是存储的有序，可重复的数据
     * 异：
     * ①ArrayList：线程不安全的，效率高；查询和添加效率高；使用Object[]数组存储数据
     * ②LinkedList：对于频繁插入、删除的数据使用此实现类比ArrayList效率高；使用双向链表存储数据
     * ③Vector：很古老的实现类，线程安全(都是同步方法)，效率低；使用Object[]数组存储数据
     */
    @Test
    public void ArrayOrLinkedTest(){
        /**
         * ArrayList解析：
         * JDK7：new ArrayList();初始化一个长度为10的Object[]数据
         * 如果添加的数据超过10个，则扩容为原来的1.5倍，复制原来的数据到新的数组中
         * JDK8：new ArrayList();初始化一个长度为0的Object[]数据
         * 在第一次添加add()的时候，新建数组长度为10，之后扩容机制和JDK1.7一致（1.5倍）
         *
         * JDK8中类似懒加载初始化，在add()中才初始化创建，节省了内存
         */
        ArrayList arrayList = new ArrayList();
        arrayList.add(null);
        /**
         *  new LinkedList();使用Node存储数据，声明了last和first属性，初始化为null
         */
        LinkedList linkedList = new LinkedList();
        linkedList.add(null);
        /**
         * new Vector();初始化一个长度为10的Object[]数据
         * 如果添加的数据超过10个，则扩容为原来的2倍，复制原来的数据到新的数组中
         */
        Vector vector = new Vector();

    }

    /**
     * Collection
     */
    @Test
    public void test1(){
        Collection coll = new ArrayList();
        coll.add("AA");
        coll.add("BB");
        coll.add(123);
        coll.add(2);
        coll.add(new Date());
        coll.add(new Person("A", 20));
//        System.out.println(coll.size());
//        System.out.println(coll.contains(new Person("A", 20)));
        coll.remove(2);
//        ((ArrayList) coll).remove(2);
        System.out.println(coll);
//        Integer[] ing = new Integer[]{123,456,789};
//        List<Integer> integers = Arrays.asList(ing);
//        System.out.println(integers);

    }

}
