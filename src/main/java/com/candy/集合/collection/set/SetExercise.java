package com.candy.集合.collection.set;

import org.junit.Test;

import java.util.HashSet;

/**
 * 面试题
 * @author Candy
 * @create 2020-08-14 11:24
 */
public class SetExercise {

    //Person重写了equals方法和hashCode方法
    @Test
    public void test(){
        HashSet hashSet = new HashSet();
        Person person1 = new Person(1001,"AA");
        Person person2 = new Person(1002,"BB");
        Person person3 = new Person(1001,"AA");

        hashSet.add(person1);
        hashSet.add(person2);
        hashSet.add(person3);
        System.out.println(hashSet);
        person1.setName("CC");
        hashSet.remove(person1);
        System.out.println(hashSet);
        hashSet.add(new Person(1001,"CC"));
        System.out.println(hashSet);
        hashSet.add(new Person(1001,"AA"));
        System.out.println(hashSet);
    }


}
