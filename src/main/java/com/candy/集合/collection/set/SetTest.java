package com.candy.集合.collection.set;

import com.candy.baseutil.compore.Person;
import org.junit.Test;

import java.util.*;

/**
 * @author Candy
 * @create 2020-08-12 16:49
 * Set:无序，不可重复的数据
 * ①.无序性：不等于随机性，根据数据的哈希值判断位置进行添加
 * ②.不可重复性：保证添加的元素按照equals判断时，返回不重复的。
 */
public class SetTest {
    /**
     * HashSet:线程不安全，可以存储null，底层是数组加链表结构存储数据(hashMap)
     *     元素添加过程：添加元素a，首先调用元素a所在类的hashCode()方法，计算元素a的哈希值，
     *     此哈希值通过"算法"计算出在底层数组中的存放位置(索引位置)，
     *     判断数组该位置上是否有元素，如果没有元素则直接添加成功，
     *     如果有其他元素则判断两个元素的hashCode是否相等，
     *          不相等则以链表的形式添加成功，相等则继续调用equals通过返回值决定是否添加成功
     *
     * 使用Set添加数据的时候，一定要重写类中的equals和hashCode()方法，并且需要保持一致性(相等的对象必须具有相等的哈希值)
     */
    @Test
    public void test(){
        Set set = new HashSet();
        set.add(null);
        set.add(123);
        set.add(888);
        set.add(new Date());
        set.add("AA");
        set.add("BB");
        Iterator iterator = set.iterator();
        while (iterator.hasNext()){
            Object next = iterator.next();
            System.out.println(next);
        }
    }

    /**
     * LinkedHashSet:属于HashSet的子类,遍历数据时按照添加顺序输出数据(对于频繁的遍历，效率高于HashSet)
     *    在添加数据的同时，还记录了两个引用属性来确定前后添加数据的顺序
     */
    @Test
    public void test2(){
        Set set = new LinkedHashSet();
        set.add(123);
        set.add(888);
        set.add(new Date());
        set.add("AA");
        set.add("BB");
        Iterator iterator = set.iterator();
        while (iterator.hasNext()){
            Object next = iterator.next();
            System.out.println(next);
        }

    }

    /**
     * TreeSet:数据必须属于同一个类，按照添加对象的指定属性排序
     *      判断添加的数据是否相等的标准为使用compareTo()方法返回是是否为0，不使用equals来判断
     *      如果为0则不添加
     *
     * 可以使用实现Comparable接口方法
     * 可以使用Comparator定制排序，可以覆盖实现Comparable接口的排序方法
     */
    @Test
    public void test3(){
//        Set set = new TreeSet();
//        set.add(123);
//        set.add(888);
//        set.add(12);
//        set.add(7);
//        set.add(1236);
        Comparator comparator = new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                if(o1 instanceof Person && o2 instanceof Person){
                    Person person1 = (Person)o1;
                    Person person2 = (Person)o2;
                    return Integer.compare(person1.getAge(),person2.getAge());
                }else{
                    throw new RuntimeException("数据类型不匹配");
                }
            }
        };
        Set set = new TreeSet(comparator);
        set.add(new Person("孙健",30));
        set.add(new Person("张凯",28));
        set.add(new Person("范瑞",45));
        set.add(new Person("京利",67));
        set.add(new Person("张涛",2));
        Iterator iterator = set.iterator();
        while (iterator.hasNext()){
            Object next = iterator.next();
            System.out.println(next);
        }
    }


}
