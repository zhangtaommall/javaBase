package com.candy.集合.collection.set.exercise;

import java.util.Date;

/**
 * @author Candy
 * @create 2020-08-14 10:44
 */
public class Employee implements Comparable {

    private String name;
    private int age;
    private MyDate birthday;
    private Date date;

    public Employee() {
    }

    public Employee(String name, int age, MyDate birthday, Date date) {
        this.name = name;
        this.age = age;
        this.birthday = birthday;
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public MyDate getBirthday() {
        return birthday;
    }

    public void setBirthday(MyDate birthday) {
        this.birthday = birthday;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", birthday=" + birthday +
                ", date=" + date +
                '}';
    }

    @Override
    public int compareTo(Object o) {
        if(o instanceof Employee){
            Employee employee = (Employee)o;
            return this.name.compareTo(employee.name);
        }
        return 0;
    }
}
