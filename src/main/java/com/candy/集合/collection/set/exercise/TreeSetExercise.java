package com.candy.集合.collection.set.exercise;

import cn.hutool.core.date.DateUtil;
import org.junit.Test;

import java.util.*;

/**
 * @author Candy
 * @create 2020-08-14 10:41
 */
public class TreeSetExercise {

    @Test
    public void testx(){
        List<Employee> employees = new ArrayList<>();
        Employee employee1 = new Employee("zhangkai",29,new MyDate(1992,5,4),DateUtil.parse("2021-02-20 08:22:22","yyyy-MM-dd HH:mm:ss"));
        Employee employee2 = new Employee("liukai",55,new MyDate(1972,8,14),DateUtil.parse("2021-02-20 15:22:22","yyyy-MM-dd HH:mm:ss"));
        Employee employee3 = new Employee("sunjian",32,new MyDate(1983,2,22),DateUtil.parse("2021-04-20 08:22:22","yyyy-MM-dd HH:mm:ss"));
        Employee employee4 = new Employee("fanrui",48,new MyDate(1979,11,1),DateUtil.parse("2020-02-20 08:22:22","yyyy-MM-dd HH:mm:ss"));
        Employee employee5 = new Employee("jingli",18,new MyDate(2001,7,19),DateUtil.parse("2021-01-20 08:22:22","yyyy-MM-dd HH:mm:ss"));
        employees.add(employee1);
        employees.add(employee2);
        employees.add(employee3);
        employees.add(employee4);
        employees.add(employee5);
        Collections.sort(employees, new Comparator<Employee>() {
            @Override
            public int compare(Employee o1, Employee o2) {
                if(o1.getDate().getTime() > o2.getDate().getTime()){
                    return -1;
                }else if(o1.getDate().getTime() < o2.getDate().getTime()){
                    return 1;
                }else{
                    return 0;
                }
            }
        });
        System.out.println(employees);

    }


    /**
     * 按照name排序
     */
    @Test
    public void test(){
        TreeSet treeSet = new TreeSet();

        Employee employee1 = new Employee("zhangkai",29,new MyDate(1992,5,4),DateUtil.parse("2021-02-20 08:22:22","yyyy-MM-dd HH:mm:ss"));
        Employee employee2 = new Employee("liukai",55,new MyDate(1972,8,14),DateUtil.parse("2021-02-20 15:22:22","yyyy-MM-dd HH:mm:ss"));
        Employee employee3 = new Employee("sunjian",32,new MyDate(1983,2,22),DateUtil.parse("2021-04-20 08:22:22","yyyy-MM-dd HH:mm:ss"));
        Employee employee4 = new Employee("fanrui",48,new MyDate(1979,11,1),DateUtil.parse("2020-02-20 08:22:22","yyyy-MM-dd HH:mm:ss"));
        Employee employee5 = new Employee("jingli",18,new MyDate(2001,7,19),DateUtil.parse("2021-01-20 08:22:22","yyyy-MM-dd HH:mm:ss"));
        treeSet.add(employee1);
        treeSet.add(employee2);
        treeSet.add(employee3);
        treeSet.add(employee4);
        treeSet.add(employee5);

        Iterator iterator = treeSet.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }

    /**
     * 按照生日排序
     */
    @Test
    public void test2(){
        TreeSet treeSet = new TreeSet(new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                if(o1 instanceof Employee && o2 instanceof Employee){
                    Employee employee1 = (Employee)o1;
                    Employee employee2 = (Employee)o2;
                    MyDate birthday = employee1.getBirthday();
                    MyDate birthday1 = employee2.getBirthday();
                    if(birthday.getYear() - birthday1.getYear() == 0){
                        if(birthday.getMonth() - birthday1.getMonth() == 0){
                            return birthday.getDay() - birthday1.getDay();
                        }
                        return birthday.getMonth() - birthday1.getMonth();
                    }
                    return birthday.getYear() - birthday1.getYear();
                }
                return 0;
            }
        });

        Employee employee1 = new Employee("zhangkai",29,new MyDate(1992,5,4),DateUtil.parse("2021-02-20 08:22:22","yyyy-MM-dd HH:mm:ss"));
        Employee employee2 = new Employee("liukai",55,new MyDate(1972,8,14),DateUtil.parse("2021-02-20 15:22:22","yyyy-MM-dd HH:mm:ss"));
        Employee employee3 = new Employee("sunjian",32,new MyDate(1983,2,22),DateUtil.parse("2021-04-20 08:22:22","yyyy-MM-dd HH:mm:ss"));
        Employee employee4 = new Employee("fanrui",48,new MyDate(1979,11,1),DateUtil.parse("2020-02-20 08:22:22","yyyy-MM-dd HH:mm:ss"));
        Employee employee5 = new Employee("jingli",18,new MyDate(2001,7,19),DateUtil.parse("2021-01-20 08:22:22","yyyy-MM-dd HH:mm:ss"));
        treeSet.add(employee1);
        treeSet.add(employee2);
        treeSet.add(employee3);
        treeSet.add(employee4);
        treeSet.add(employee5);
        Iterator iterator = treeSet.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }

}
