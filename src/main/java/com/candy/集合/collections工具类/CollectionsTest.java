package com.candy.集合.collections工具类;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Candy
 * @create 2020-08-19 17:52
 * 操作Collection和Map的工具类：Collections
 */
public class  CollectionsTest {

    /**
     * reverse(List)：反转 List 中元素的顺序
     * shuffle(List)：对 List 集合元素进行随机排序
     * sort(List)：根据元素的自然顺序对指定 List 集合元素按升序排序
     * sort(List，Comparator)：根据指定的 Comparator 产生的顺序对 List 集合元素进行排序
     * swap(List，int， int)：将指定 list 集合中的 i 处元素和 j 处元素进行交换
     *
     * Object max(Collection)：根据元素的自然顺序，返回给定集合中的最大元素
     * Object max(Collection，Comparator)：根据 Comparator 指定的顺序，返回 给定集合中的最大元素
     * Object min(Collection)
     * Object min(Collection，Comparator)
     * int frequency(Collection，Object)：返回指定集合中指定元素的出现次数
     * void copy(List dest,List src)：将src中的内容复制到dest中
     * boolean replaceAll(List list，Object oldVal，Object newVal)：使用新值替换 List 对象的所有旧
     */
    @Test
    public void test(){
        List list = new ArrayList();
        list.add(12);
        list.add(43);
        list.add(869);
        list.add(444);
        list.add(0);
        list.add(-225);
        System.out.println("调用reverse()方法之前------>"+list);
        Collections.reverse(list);//反转 List 中元素的顺序
        System.out.println("调用reverse()方法之后------>"+list);

    }

}
