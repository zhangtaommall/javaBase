package com.candy.集合.map;

import org.junit.Test;

import java.util.*;

/**
 * @author Candy
 * @create 2020-08-19 11:43
 * Map定义的常用方法-遍历
 */
public class MapMethodTest {


    @Test
    public void test(){
        Map map = new HashMap();
        map.put("AA",123);
        map.put("BB",34);
        map.put("CC",444);
        map.put("DD",666);
        //获取所有的key：keySet()
        Iterator iterator = map.keySet().iterator();
        while(iterator.hasNext()){
            Object next = iterator.next();
            System.out.println(next+"====="+map.get(next));
        }
        for (Object o : map.keySet()) {
            System.out.println(o+"-----"+map.get(o));
        }
        //获取所有的value：values()
        Collection values = map.values();
        for (Object value : values) {
            System.out.println(value);
        }
        //获取所有的key=value：entrySet()
        Set set = map.entrySet();
        for (Object o : set) {
            Map.Entry entry = (Map.Entry)o;
            System.out.println(entry.getKey()+"----->"+entry.getValue());
        }

    }



}
