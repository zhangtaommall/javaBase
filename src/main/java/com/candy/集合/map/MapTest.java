package com.candy.集合.map;

import org.junit.Test;

import java.util.*;

/**
 * @author Candy
 * @create 2020-08-14 14:24
 *
 * Map:双列数据,存储key-value对的数据
 *      HashMap:线程不安全，效率高;可以存储null的key和value
 *          LinkedHashMap:保证再遍历数据的时候，实现按照添加顺序实现遍历(对于频繁的遍历，效率高于HashMap)
 *                  原因：在添加数据的同时，还记录了两个引用属性来确定前后添加数据的顺序
 *      TreeMap:按照添加的key和value进行排序，实现排序遍历(是按照key来排序的)
 *      Hashtable:线程安全，效率低;不可以存储null的key和value
 *          Properties:用来处理配置文件读取;他的key和value都是String类型
 *
 *  HashMap底层:使用数组+链表(JDK7及之前)
 *              数组+链表+红黑树(JDK8)
 *
 */
public class MapTest {

    /**
     *  Map中的key:无序的，不可重复的，使用Set存储所有的key
     *  Map中的value:无序的，可重复的，使用Collection存储所有的value
     *      一个键值对：key-value构成了一个Entry对象(两个属性分别为key和value)
     *  Map中的Entry:无序的，不可重复的，使用Set存储所有的Entry
     *
     *
     *  HashMap实现原理？JDK7版本
     *      1.HashMap map = new HashMap();//加载因子为0.75
     *      实例化之后创建了长度为16的数组，类型为Entry[] table
     *      2.map.put(k1,v1);
     *      首先调用k1所在类的hashCode()方法计算哈希值，计算得到Entry在数组中的存放位置
     *      如果此位置数据为空，则此时k1,v1添加成功(Entry添加成功)
     *      如果次位置数据不为空(存在一个或多个数据(链表结构)),比较需要存放数据的k1与已经存在的数据的k1值(比较哈希)
     *          如果哈希值两者不相等，则直接添加成功
     *          如果哈希值两者相等，则继续调用k1.equals(已存在的k)
     *              方法如果不相等，添加成功,
     *              方法如果相等，使用v1替换相等k的值(使用key修改功能)
     *
     *      扩容问题：默认扩容为原来的2倍
     *
     *      JDK8版本
     *      1.HashMap map = new HashMap();//加载因子为0.75
     *      实例化之后并没有创建数据
     *      2.map.put(k1,v1);(储存数组方法数据+链表+红黑树)
     *      首次调用put方法时，创建一个长度为16的数据，类型为Node[]
     *      当数组某个索引位置上的元素以链表形式存储的数据大于8个，并且当前数组长度超过64个，此时索引位置上的所有数据改为"红黑树"存储
     *
     *      MIN_TREEIFY_CAPACITY = 64       表示底层数组长度超过此值，则把链表的存储方式改为红黑树(和TREEIFY_THRESHOLD = 8搭配使用)
     *      TREEIFY_THRESHOLD = 8           表示数组中某个索引下的链表结构超过8个(考虑使用红黑树存储,和MIN_TREEIFY_CAPACITY = 64搭配使用)
     *      DEFAULT_LOAD_FACTOR = 0.75f     表示HashMap默认的加载因子
     *      DEFAULT_INITIAL_CAPACITY = 16   表示HashMap默认的的初始化容量16
     *      MAXIMUM_CAPACITY = 1 << 30      表示HashMap最大支持容量
     *      threshold                       表示扩容临界值 = 容量*加载因子(16*0.75=12)
     *
     *
     *
     *      LinkedHashMap的底层实现原理：
     *      1.重写了HashMap的newNode()方法
     *      2.LinkedHashMap的Entry属性继承了HashMap下的Node类，
     *          并且多了2个属性：before和after用了记录添加数据的顺序
     *      3.对于频繁的遍历使用效率高
     *
     */
    @Test
    public void test(){
        Map map = new HashMap();
        map.put(null,123);
        map.put(null,null);
//        Map map2 = new Hashtable();
//        map2.put(null,null);//会报错
        System.out.println(16 << 1);
        Map linkedHashMap = new LinkedHashMap();

    }



}
